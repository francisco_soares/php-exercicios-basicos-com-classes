<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Exercício 22 - Envio de curriculo</title>

    <!-- Style -->
    <link rel="stylesheet" href="../assets/style/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/style/style.css?v=<?= time(); ?>">
</head>

<body>

    <!-- 22º* - Crie um formulário com os campos: Nome, E-mail, Telefone e Enviar CV        
      O todos os campos devem ser validado        
      O campo ENVIAR CV deve aceitar arquivos com as seguintes extensões: DOC e PDF        
      Salvar os arquivos em uma pasta chamada CV -->

    <div class="container h-100">
        <div class="row h-100">
            <div class="formulario col-md-6 offset-md-3 align-self-center" id="content">
                <h3 class="text-center font-weight-bold">Envio de Curriculo</h3>
                <p class="text-center">Informe todos os dados no formulário abaixo para enviar seu Currículo.</p>

                <form action="../Config/controlador.php" method="POST" id="sendForm" enctype="multipart/form-data">
                    <input type="hidden" name="exercicio" value="exercicio-22">

                    <div class="mb-2">
                        <label for="name">Nome: <span>*</span></label>
                        <input type="text" name="name" id="name" class="form-control" placeholder="Informe o seu nome.">
                    </div>

                    <div class="mb-2">
                        <label for="name">E-mail: <span>*</span></label>
                        <input type="email" name="email" id="email" class="form-control" placeholder="Informe o seu melhor e-mail.">
                    </div>

                    <div class="mb-2">
                        <label for="whatsapp">WhatsApp: <span>*</span></label>
                        <input type="tel" name="whatsapp" id="whatsapp" class="form-control" placeholder="Informe o seu WhatsApp.">
                    </div>

                    <div class="mb-4">
                        <label for="cv">Curriculo: <span>*</span></label>
                        <!-- <input type="file" name="cv" id="cv" class="form-control" placeholder="Enviei seu CV"> -->
                        <div class="custom-file">
                            <input type="file" name="cv" class="custom-file-input" id="cv" aria-describedby="cv">
                            <label class="custom-file-label" id="label_input_file" for="cv">Enviar arquivo</label>
                        </div>
                    </div>

                    <div class="mb-4">
                        <input type="submit" value="Enviar" class="btn btn-lg btn-outline-danger font-weight-bold w-100" id="btn_send">
                    </div>

                </form>

                <?php if (isset($_GET['result']) && $_GET['result'] == "error") { ?>
                    <div class="bg-warning p-3 rounded text-center result">
                        <h3 class="text-dark">OPSS...</h3>
                        <p class="text-dark"><strong>TODOS</strong> os campos devem ser preenchido!</p>
                    </div>
                <?php } ?>
                <?php if (isset($_GET['result']) && $_GET['result'] == "errorCV") { ?>
                    <div class="bg-warning p-3 rounded text-center result">
                        <h3 class="text-dark">OPSS...</h3>
                        <p class="text-dark">Opss... Você deve enviar arquivos em <strong>PDF ou WORD!</strong></p>
                    </div>
                <?php } ?>
                <?php if (isset($_GET['result']) && $_GET['result'] == "success") { ?>
                    <div class="bg-success p-3 rounded result">
                        <h3 class="text-white font-weight-bold text-center">Resultado</h3>
                        <p class="text-white text-center">Os dados abaixo foram enviado com sucesso!</p>
                        <p class="text-white"><strong>Nome:</strong> <?= $_GET['name']; ?></p>
                        <p class="text-white"><strong>E-mail:</strong> <?= $_GET['email']; ?></p>
                        <p class="text-white"><strong>WhatsApp:</strong> <?= $_GET['whatsapp']; ?></p>
                        <p class="text-white"><strong>Nome CV:</strong> <?= $_GET['cv']; ?></p>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <!-- Scripts -->
    <script src="../assets/style/bootstrap/js/bootstrap.min.js"></script>
    <script src="../assets/js/jquery-3.6.0.min.js"></script>

    <script>
        $(document).ready(function() {
            /** ==== [INÍCIO]: Váriaveis declaradas */
            let name = $("#name").val();
            let email = $("#email").val();
            let whatsapp = $("#whatsapp").val();
            let cv = $("#cv").val();
            /** [FIM]: ==== */

            /** ==== [INÍCIO]: Bloco que verifica assim que acessa a página se o campo input do form está vázio. */
            if (name == "" || email == "" || whatsapp == "" || cv == "") {
                $("#btn_send").prop("disabled", true);
            }
            /** [FIM]: ==== */

            $("#cv").change(function(e) {

                /** ==== [INÍCIO]: Váriaveis declaradas */
                let email_keyup = $("#email").val();
                let whatsapp_keyup = $("#whatsapp").val();
                let cv_keyup = e.target.files[0].name;
                let fileType = e.target.files[0].type;

                let arrFile = [
                    "application/pdf",
                    "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                    "application/msword"
                ];
                /** [FIM]: ==== */

                // Verifico se a DIV ID errorForm está sendo apresentada e a removo
                if ($("#errorForm")) {
                    $("errorForm").remove();
                }

                if ($(".result")) {
                    $(".result").remove();
                }

                /* Verifico se o tipo do arquivo não existe, se não existir, entra no if() */
                if ($.inArray(fileType, arrFile) == -1) {

                    // Crio a DIV ID errorForm
                    let errorFile = $("<div>", {
                        id: "errorForm",
                        class: "bg-danger p-3 rounded text-center"
                    });

                    // Desabilito o botão de enviar
                    $("#btn_send").prop("disabled", true);

                    // Insiro um texto na DIV ID errorForm
                    errorFile.html('<h3 class="text-white">Éhhh...</h3><p class="text-white">OPSSS... Formato do arquivo incorreto, favor enviar arquivo em <strong>PDF ou Word</strong></p>');

                    // Mostro a DIV ID errorForm ao final mas dentro da DIV ID content
                    $("#content").append(errorFile);

                    // Limpo o campo input do CV
                    $("#cv").val('');

                    /* Se o formato for diferente de PDF e DOC, eu zero o campo */
                    $("#label_input_file").html("Enviar arquivo");
                    /** Continuar a validação quando é enviado um arquivo diferente de PDF e DOC */
                } else {

                    if ($("#errorForm")) {
                        $("#errorForm").remove();
                    }

                    /* No momento que subo a imagem, é pego no nome da mesma e altera o campo #label_input_file*/
                    $("#label_input_file").html(cv_keyup);

                    $("#btn_send").prop("disabled", false);
                }




            });

            $("#sendForm").keyup(function() {
                /** ==== [INÍCIO]: Váriaveis declaradas */
                let name_keyup = $("#name").val();
                let email_keyup = $("#email").val();
                let whatsapp_keyup = $("#whatsapp").val();
                let cv_keyup = $("#cv").val();
                // let cv_keyup = $("#input[name='cv[]']").val();


                console.log(cv_keyup.length);
                /** [FIM]: ==== */

                // Verifico se a DIV ID errorForm está sendo apresentada e a removo
                if ($("#errorForm")) {
                    $("#errorForm").remove();
                }

                if ($(".result")) {
                    $(".result").remove();
                }

                // Verifico se algum dos campos não está preenchido, se sim, entra na condição.
                if (name_keyup == "" || email_keyup == "" || whatsapp_keyup == "" || cv_keyup.length == 0) {

                    // Criado uma div para apresentar o erro.
                    let returnError = $("<div>", {
                        id: "errorForm",
                        class: "bg-danger p-3 rounded text-center"
                    });

                    /** Desabilito o botão de enviar */
                    $("#btn_send").prop("disabled", true);

                    // returnError.html('<h3 class="text-white">Éhhh...</h3><p class="text-white">Insira apenas <strong>Números separados por ponto(.).</strong></p>');
                } else {
                    $("#btn_send").prop("disabled", false);
                }
            });
        });
    </script>
</body>

</html>