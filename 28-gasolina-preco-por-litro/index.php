<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gasolina - preço por litro</title>

    <!-- Style -->
    <link rel="stylesheet" href="../assets/style/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/style/style.css?v=<?= time(); ?>">

    <link href="https://fonts.googleapis.com/css2?family=Kanit:ital,wght@0,200;0,300;0,500;0,700;0,800;1,300&family=Montserrat:ital,wght@0,100..900;1,100..900&display=swap" rel="stylesheet">

    <style>
        body {
            font-family: "Montserrat", serif;
            color: #666;
        }

        /* Esconde a bolinha do input radio */
        input[type="radio"] {
            display: none;
        }

        .btn-group label {
            margin: 4px;
        }

        /* Estilo adicional para o botão selecionado */
        .btn.active {
            background-color: #007bff;
            color: #fff;
            border-color: #007bff;
        }
    </style>
</head>

<body>
    <div class="container h-100">
        <div class="row h-100 d-flex justify-content-center align-items-center">
            <div class="formulario col-6-md " id="content">

                <h3 class="text-center font-weight-bold">( Gasolina )</h3>
                <p class="text-center">Escolha uma das opções abaixo.</p>

                <div class="btn-group d-flex justify-content-center mb-5" data-toggle="buttons">

                    <label class="btn btn-success btn_checked">
                        <input type="radio" name="options" id="option1" autocomplete="off" value="litro"> Calculo por litro
                    </label>
                    <label class="btn btn-success btn_checked">
                        <input type="radio" name="options" id="option2" autocomplete="off" value="km"> Calculo por KM
                    </label>
                </div>


                <form action="" method="POST" id="sendForm" class="mt-5 sendForm">
                    <input type="hidden" name="exercicio" id="exercicio" value="exercicio-28">

                    <div class="row mb-4">
                        <div class="col-md">
                            <label for="distancia" class="label_dist_lt">Distância percorrida</label>
                            <input type="text" name="distancia" id="distancia" class="form-control" placeholder="km">
                        </div>
                        <div class="col-md col_consumo">
                            <label for="consumo">Consumo médio do veículo</label>
                            <input type="text" name="consumo" id="consumo" class="form-control" placeholder="12">
                        </div>
                        <div class="col-md">
                            <label for="preco_litro">Preço por litro</label>
                            <input type="text" name="preco_litro" id="preco_litro" class="form-control" placeholder="R$ 5,87">
                        </div>
                    </div>

                    <div class="mb-4">
                        <input type="submit" value="Calcular" class="btn btn-lg btn-warning font-weight-bold w-100" id="btn_send">
                    </div>
                </form>

                <div class="result-error text-center">
                    <h5 class="text-danger font-weight-bold">Opssss...</h5>
                    <p class="text-danger"><strong>Todos</strong> os campos devem ser preenchido!</p>
                </div>


                <div class="result-success text-center">
                    <h3 class="font-weight-bold">Resultado</h3>
                    <p></p>

                    <table class="table table-bordered">
                        <thead class="thead-light">
                            <tr>
                                <th scope="col" class="th_km">KM</th>
                                <th scope="col" class="th_consumo">Consumo</th>
                                <th scope="col">Preço</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="tr_view">
                                
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- Scripts -->
    <script src="../assets/js/jquery-3.6.0.min.js"></script>
    <script src="../assets/style/bootstrap/js/bootstrap.min.js"></script>
    <!-- Popper.js (dependência do Bootstrap 4) -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>

    <!-- JavaScript do Bootstrap -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>

    <script>
        $(document).ready(function() {
            $("input[name='preco_litro']").mask("R$ 0,00");
            $("input[name='consumo']").mask("00");
            $("input[name='distancia']").mask("00000");

            var radio_select;

            $(".btn_checked").on("click", function() {

                $("input[type='text']").val('');

                $(".result-error").css({
                    "display": "none"
                });

                $(".result-success").css({
                    "display": "none"
                });

                $(".tr_view").html('');

                $("#sendForm").show();
                radio_select = $(this).find("input[type='radio']").val();
                if (radio_select == "km") {
                    $('.col_consumo').show();
                    $('.label_dist_lt').html('Distância percorrida');
                    $("input[name='distancia']").attr({placeholder: "KM"});
                    $('.th_km').html('KM');
                    $('.th_consumo').show();
                } else {
                    $('.col_consumo').hide();
                    $('.label_dist_lt').html('Litros');
                    $("input[name='distancia']").attr({placeholder: "Litros"});
                    $('.th_km').html('Litros');
                    $('.th_consumo').hide();
                }
            });

            // console.log($("input[name='options']:checked").val());

            $("#sendForm").on("submit", function(e) {
                e.preventDefault();

                $(".result-error").css({
                    "display": "none"
                });

                $(".result-success").css({
                    "display": "none"
                });

                let gas = $("#preco_litro");
                let preco_litro = gas.val().replace("R$", "").trim().replace(",", ".");
                let consumo = $("input[name='consumo']");
                let distancia = $("input[name='distancia']");
                let exercicio = $("#exercicio");

                if (preco_litro.length == 0 || distancia.val().length == 0) {
                    $(".result-error").show();
                    return;
                }

                if (radio_select == "km") {
                    if (consumo == '' || consumo == undefined) {
                        $(".result-error").show();
                        return;
                    }
                }

                data = {
                    preco_litro: preco_litro,
                    consumo: consumo.val(),
                    exercicio: exercicio.val(),
                    radio_select: radio_select,
                    distancia: distancia != undefined ? distancia.val() : ''
                }

                // let formData = new FormData(this);

                $.ajax({
                    url: "../Config/controlador.php",
                    type: "POST",
                    data: data,
                    // contentType: false,
                    // processData: false,
                    beforeSend: () => {
                        $("#btn_send").attr("disabled", "disabled").val("Enviando...");
                        $("input[name='preco_litro']").attr("disabled", "disabled");
                        $("input[name='consumo']").attr("disabled", "disabled");
                    },
                    success: (response) => {
                        if (response.status == true) {

                            if(radio_select == 'litro') {
                                $('.th_consumo').hide();
                                $(".tr_view").html(`
                                <td>${response.litros}</td>
                                <td>R$ ${response.preco}</td>
                                `);
                            } else {
                                $('.th_consumo').show();
                                $(".tr_view").html(`
                                <td>${response.km}</td>
                                <td>${response.consumo}</td>
                                <td>R$ ${response.preco}</td>
                                `);
                            }
                            
                            $("input[name='preco_litro']").attr("disabled", false);
                            $("input[name='consumo']").attr("disabled", false);
                            $("#btn_send").attr("disabled", false).val("Calcular");
                            $(".result-success").show();
                        }
                    }
                });
            });
        });
    </script>
</body>

</html>