<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Biblioteca da Universidade</title>

    <!-- Style -->
    <link rel="stylesheet" href="../assets/style/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/style/style.css?v=<?= time(); ?>">
</head>

<body>

    <div class="container h-100">
        <div class="row h-100">
            <div class="formulario col-md-6 offset-md-3 align-self-center" id="content">
                <h3 class="text-center font-weight-bold">Biblioteca da Universidade</h3>
                <p class="text-center">Preencha o formulário abaixo para conseguir o livro emprestado.</p>

                <form action="../Config/controlador.php" method="POST" id="sendForm">

                    <input type="hidden" name="exercicio" value="exercicio-16">

                    <div class="mb-4">
                        <span class="font-weight-bold d-block">Você é?</span>
                        <div class="form-check form-check-inline">
                            <input type="radio" name="choice" id="student" value="student" class="form-check-input">
                            <label for="student" class="form-check-label mr-2">Aluno</label>

                            <input type="radio" name="choice" id="teacher" value="teacher" class="form-check-input">
                            <label for="teacher" class="form-check-label">Professor</label>
                        </div>
                    </div>

                    <div class="mb-4">
                        <label for="name" class="font-weight-bold">Informe seu nome</label>
                        <input type="text" name="name" id="name" class="form-control" placeholder="Ex: Saitama">
                    </div>

                    <div class="mb-4">
                        <label for="book" class="font-weight-bold">Informe o nome do livro que vai pegar emprestado</label>
                        <input type="text" name="book" id="book" class="form-control" placeholder="Ex: Um estudo em Vermelho">
                    </div>

                    <div class="mb-4">
                        <input type="submit" value="Enviar" class="btn btn-lg btn-outline-danger font-weight-bold w-100" id="btn_send">
                    </div>
                </form>

                <?php if (isset($_GET['result']) && $_GET['result'] == "error") { ?>
                    <div class="bg-warning p-3 rounded text-center result">
                        <h3 class="text-dark">OPSS...</h3>
                        <p class="text-dark"><strong>TODOS</strong> os campos devem ser preenchido!</p>
                    </div>
                <?php } ?>

                <?php if (isset($_GET['name']) && !empty($_GET['name']) && isset($_GET['book']) && !empty($_GET['book']) && isset($_GET['days']) && !empty($_GET['days'])) { ?>

                    <div class="bg-success p-3 rounded result">
                        <h3 class="text-white text-center">Resultado</h3>
                        <p class="text-white">
                            <strong>Nome:</strong> <?= $_GET['name']; ?><br>
                            <strong>Livro:</strong> <?= $_GET['book']; ?><br>
                            <strong>Qtd de dias emprestado:</strong> <?= $_GET['days']; ?> dia(s)<br>
                        </p>
                    </div>

                <?php } ?>
            </div>
        </div>
    </div>

    <!-- Scripts -->
    <script src="../assets/style/bootstrap/js/bootstrap.min.js"></script>
    <script src="../assets/js/jquery-3.6.0.min.js"></script>
    <script>
        // Code here
        /**
         * Faço uma validação assim que carrega toda a página.
         * Verifico se os campos #name ou #book são vázios.
         * Caso atenda a esse requisito, o btn de enviar é desabilitado.
         * 
         * Sendo assim, quando carrega a pagina o btn já vem desabilitado.
         */
        $(document).ready(function() {
            let name = $("#name").val().trim()
            let book = $("#book").val()

            if (name = "" || book == "") {
                $("#btn_send").prop("disabled", true)
            }
        })

        /**
         * Faço uma validação nos campos de radio.
         * Verifico se o mesmo foi checkado, e também verifico se os input #name e #book é diferente de vázio.
         * Caso atenda a esses requisitos, habilita o btn de enviar.
         */
        $("input:radio[name=choice]").click(function() {
            if ($("input:radio[name=choice]").is(":checked") == true && $("#name").val() != "" && $("#book").val() != "") {
                $("#btn_send").prop("disabled", false)
            }
        })

        /**
         * Validação que ocorre no momento que está sendo digitado as informações.
         * Dentro terá mais explicações.
         */
        $("#sendForm").keyup(function() {

            // Pego os valores dos campos
            let name = $("#name").val()
            let book = $("#book").val()

            /**
             * Faço uma verificação nos campos name, book e radios.
             * Assim desabilito o mesmo se existir ou não.
             */
            if ((name || !name) || (book || !book) || ($("input:radio[name=choice]").is(":checked") == true || $("input:radio[name=choice]").is(":checked") == false)) {
                $("#btn_send").prop("disabled", false)
            }

            /**
             * Faço uma validação para verificar se algum dos campos está vazio ou retorna false.
             * Caso atenda alguma dessa condições, desabilita o btn enviar.
             */
            if (name == "" || book == "" || $("input:radio[name=choice]").is(":checked") == false) {
                $("#btn_send").prop("disabled", true)
            }

            /**
             * Verifico dentro do campo #name se apresenta espaços em branco acima de 1, caso atenda, o mesmo retira todos os espaços em branco.
             * Assim, deixando apenas o que você digitou.
             */
            if (name.indexOf("  ") > -1) {
                $("#name").val(name.trim())
            }

            /**
             * Verifico dentro do campo #book se apresenta espaços em branco acima de 1, caso atenda, o mesmo retira todos os espaços em branco.
             * Assim, deixando apenas o que você digitou.
             */
            if (book.indexOf("  ") > -1) {
                $("#book").val(book.trim())
            }

            if ($("#errorForm")) {
                $("#errorForm").remove()
            }

            /**
             * Verifico se tem mais de um espaço em branco em seguida.
             * Assim desabilita o btn enviar.
             * 
             * Também é mostrado o erro
             */
            if (name.indexOf("  ") == 0 || book.indexOf("  ") == 0) {
                $("#btn_send").prop("disabled", true)

                let returnError = $("<div>", {
                    id: 'errorForm',
                    class: 'bg-danger p-3 rounded text-center'
                })

                returnError.html('<h3 class="text-white">Éhhh...</h3><p class="text-white">Todos os campos são obrigatórios. <strong>Digite algo!</strong></p>')

                $("#content").append(returnError)

                if ($('.result')) {
                    $('.result').remove()
                }

                $("#btn_send").attr('title', 'Digite algo nos campos para habilitar esse botão')
            }
        })
    </script>
</body>

</html>