<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Farenheit para Celsius</title>

    <!-- Style -->
    <link rel="stylesheet" href="../assets/style/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/style/style.css?v=<?= time(); ?>">
</head>

<body>
    <div class="container h-100">
        <div class="row h-100">
            <div class="formulario col-md-6 offset-md-3 align-self-center" id="conteudo">
                <h3 class="font-weight-bold text-uppercase">Farenheit para Celsius</h3>
                <p class="mb-5">Converta Farenheit para Celsius</p>

                <form action="../Config/controlador.php" method="POST" id="enviarForm">
                    <input type="hidden" name="exercicio" value="exercicio-7">
                    <div class="mb-4">
                        <label for="num" class="font-weight-bold">Informe o número</label>
                        <input type="text" name="num" id="num" class="form-control form-control-lg" placeholder="Ex: 10">
                    </div>

                    <div class="mb-4">
                        <input type="submit" value="Enviar" class="btn btn-lg btn-outline-danger w-100 font-weight-bold text-uppercase">
                    </div>
                </form>

                <?php if (isset($_GET['result']) && $_GET['result'] == 'error') { ?>
                    <div class="bg-warning p-3 rounded text-center result">
                        <h3 class="text-dark">OPSS...</h3>
                        <p class="text-dark"><strong>TODOS</strong> os campos devem ser preenchido!</p>
                    </div>
                <?php } elseif (isset($_GET['celsius'])  && isset($_GET['farenheit'])) { ?>
                    <div class="bg-success p-3 rounded text-center result">
                        <h3 class="text-white">Resultado</h3>
                        <p class="text-white">
                            <strong>Farenheit: </strong> <?= $_GET['farenheit']; ?><br>
                            <strong>Celsius: </strong> <?= $_GET['celsius']; ?>
                        </p>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>

    <!-- Scripts -->
    <script src="../assets/style/bootstrap/js/bootstrap.min.js"></script>
    <script src="../assets/js/jquery-3.6.0.min.js"></script>

    <script>
        $('#enviarForm').submit(function(e) {


            let num = parseFloat($('#num').val())
            let out

            if (isNaN(num)) {

                e.preventDefault()

                if ($('#errorForm')) {
                    $('#errorForm').remove()
                }

                let returnError = $('<div>', {
                    id: 'errorForm',
                    class: 'bg-danger p-3 rounded text-center'
                })

                returnError.html('<h3 class="text-white">Éhhh...</h3><p class="text-white">Insira apenas <strong>Números</strong>!</p>')

                $('#conteudo').append(returnError)

                if ($('.result')) {
                    $('.result').remove()
                }
            }

            out = setTimeout(alertOut, 4000)
        })

        function alertOut() {
            $('#errorForm').fadeOut(800)
        }
    </script>

</body>

</html>