<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Salário do Mês com os Descontros</title>

    <!-- Style -->
    <link rel="stylesheet" href="../assets/style/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/style/style.css?v=<?= time(); ?>">
</head>

<body>
    <div class="container h-100">
        <div class="row h-100">
            <div class="formulario col-md-6 offset-md-3 align-self-center" id="contents">
                <h3 class="text-center font-weight-bold">Salário do Mês com os Descontros</h3>
                <p class="text-center mb-5">Informe abaixo suas horas trabalhadas e seu valor por hora.</p>

                <form action="../Config/controlador.php" method="POST" id="sendForm">
                    <input type="hidden" name="exercicio" value="exercicio-12">

                    <div class="mb-4">
                        <label for="value_hour" class="font-weight-bold">Insira o valor da sua hora</label>
                        <input type="text" name="value_hour" id="value_hour" class="form-control" placeholder="Ex: 85.00">
                    </div>
                    <div class="mb-4">
                        <label for="month_worked" class="font-weight-bold">Insira a quantidade de horas trabalhada no mes:</label>
                        <input type="text" name="month_worked" id="month_worked" class="form-control" placeholder="Ex: 200">
                    </div>
                    <div class="row mb-4">
                        <div class="col-md-4">
                            <label for="ir">IR:</label>
                            <input type="text" name="ir" id="ir" class="form-control" value="<?= isset($_GET['ir']) ? 'R$ ' . $_GET['ir'] : ''; ?>" disabled>
                        </div>
                        <div class="col-md-4">
                            <label for="inss">INSS:</label>
                            <input type="text" name="inss" id="inss" class="form-control" value="<?= isset($_GET['inss']) ? 'R$ ' . $_GET['inss'] : ''; ?>" disabled>
                        </div>
                        <div class="col-md-4">
                            <label for="syndicate">Sindicato</label>
                            <input type="text" name="syndicate" id="syndicate" class="form-control" value="<?= isset($_GET['syndicate']) ? 'R$ ' . $_GET['syndicate'] : ''; ?>" disabled>
                        </div>

                        <div class="col-md-6">
                            <label for="gross_salary">Salário Bruto</label>
                            <input type="text" name="gross_salary" id="gross_salary" class="form-control" value="<?= isset($_GET['gross_salary']) ? 'R$ ' . $_GET['gross_salary'] : ''; ?>" disabled>
                        </div>
                        <div class="col-md-6">
                            <label for="net_salary">Salário Líquido</label>
                            <input type="text" name="net_salary" id="net_salary" class="form-control" value="<?= isset($_GET['net_salary']) ? 'R$ ' . $_GET['net_salary'] : ''; ?>" disabled>
                        </div>
                    </div>
                    <div class="mb-4">
                        <input type="submit" value="Enviar" id="btn_send" class="btn btn-lg btn-outline-dark w-100 font-weight-bold text-uppercase">
                    </div>
                </form>

                <?php if (isset($_GET['result']) && $_GET['result'] == "error") { ?>
                    <div class="bg-warning p-3 rounded text-center result">
                        <h3 class="text-dark">OPSS...</h3>
                        <p class="text-dark"><strong>TODOS</strong> os campos devem ser preenchido ou não pode ser <strong>0</strong> (zero)!</p>
                    </div>
                <?php } ?>

                <?php if ((isset($_GET['gross_salary']) && !empty($_GET['gross_salary'])) && (isset($_GET['net_salary']) && !empty($_GET['net_salary'])) && (isset($_GET['ir']) && !empty($_GET['ir'])) && (isset($_GET['inss']) && !empty($_GET['inss'])) && (isset($_GET['syndicate']) && !empty($_GET['syndicate']))) { ?>

                    <div class="bg-success pt-3 pb-3 pr-5 pl-5 rounded  result">
                        <h3 class="text-white text-center font-weight-bold">Resultado</h3>
                        <p class="text-white">
                            <strong>IR: </strong> R$ <?= $_GET['ir']; ?> kg<br>
                            <strong>INSS: </strong> R$ <?= $_GET['inss']; ?> kg<br>
                            <strong>Sindicato: </strong> R$ <?= $_GET['syndicate']; ?><br>
                            <strong>Salário Bruto: </strong> R$ <?= $_GET['gross_salary']; ?><br>
                            <strong>Salário Líquido: </strong> R$ <?= $_GET['net_salary']; ?>
                        </p>
                    </div>

                <?php } ?>
            </div>
        </div>
    </div>

    <!-- Scripts -->
    <script src="../assets/style/bootstrap/js/bootstrap.min.js"></script>
    <script src="../assets/js/jquery-3.6.0.min.js"></script>

    <script>
        $('#sendForm').keyup(function() {
            let value_hour = $("#value_hour").val()
            let month_worked = $("#month_worked").val()
            let value_hour_float = value_hour
            let month_worked_float = month_worked
            let gross_salary
            let ir
            let inss
            let syndicate
            let total_discount
            let net_salary

            if ((value_hour || !value_hour) || (month_worked || !month_worked)) {
                $('#btn_send').prop('disabled', false)
            }

            if ($("#errorForm")) {
                $("#errorForm").remove()
            }

            if (isNaN(value_hour) || isNaN(month_worked)) {

                $('#btn_send').prop('disabled', true)

                let returnError = $("<div>", {
                    id: 'errorForm',
                    class: 'bg-danger p-3 rounded text-center'
                })

                returnError.html('<h3 class="text-white">Éhhh...</h3><p class="text-white">Insira apenas <strong>Números</strong>!</p>')

                $("#contents").append(returnError)
            }

            if (value_hour_float > 0 && month_worked_float > 0) {
                // Calc Gross Salary
                gross_salary = month_worked_float * value_hour_float
                // Calc IR
                ir = 11 * gross_salary / 100
                // Calc INSS
                inss = 8 * gross_salary / 100
                // Calc SYNDICATE
                syndicate = 5 * gross_salary / 100
                // Calc total discount
                total_discount = ir + inss + syndicate
                // Calc net salary
                net_salary = gross_salary - total_discount

                // Insiro o valor nos campos inputs já formatado com a moeda REAL/BR
                $('#gross_salary').val(gross_salary.toLocaleString('pt-br', {
                    style: 'currency',
                    currency: 'BRL'
                }))
                $('#ir').val(ir.toLocaleString('pt-br', {
                    style: 'currency',
                    currency: 'BRL'
                }))
                $('#inss').val(inss.toLocaleString('pt-br', {
                    style: 'currency',
                    currency: 'BRL'
                }))
                $('#syndicate').val(syndicate.toLocaleString('pt-br', {
                    style: 'currency',
                    currency: 'BRL'
                }))
                $('#net_salary').val(net_salary.toLocaleString('pt-br', {
                    style: 'currency',
                    currency: 'BRL'
                }))
            }
        })
    </script>

</body>

</html>