<?php
require_once "../vendor/autoload.php";

// Recebo a url da API e seto na variavel $url_api.
$url_api = 'https://swapi.dev/api/people/?page=2';

$id_actor = 1;

$actor = new \Config\Model\consultaAPI($url_api);

?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Consumir API</title>

    <!-- Style -->
    <link rel="stylesheet" href="../assets/style/bootstrap/css/bootstrap.min.css">
    <!-- <link rel="stylesheet" href="../assets/style/style.css?v=<?= time(); ?>"> -->
</head>

<body style="background-color: #fff;">


    <div class="container" style="background-color: #fff;">
        <div class="row">
            <div class="col-md-12">
                <table class="table">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Full Name</th>
                            <th scope="col">Gender</th>
                            <th scope="col">Films</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($actor->getResult()->results as $actor) : ?>
                            <tr>

                                <th scope="row"><?= $id_actor ?></th>
                                <td><?= $actor->name; ?></td>
                                <td><?= $actor->gender; ?></td>
                                <td>
                                    <?php
                                    foreach ($actor->films as $films) :
                                        $objFilmes = new \Config\Model\consultaAPI($films);
                                        echo $objFilmes->getResult()->title . "<br>";
                                    endforeach;
                                    ?>
                                </td>
                            </tr>
                        <?php
                            $id_actor++;
                        endforeach;
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- Scripts -->
    <script src="../assets/style/bootstrap/js/bootstrap.min.js"></script>
    <script src="../assets/js/jquery-3.6.0.min.js"></script>
    <script>
    </script>
</body>

</html>