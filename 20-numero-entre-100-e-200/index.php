<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ler numero entre 100 e 200</title>

    <!-- Style -->
    <link rel="stylesheet" href="../assets/style/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/style/style.css?v=<?= time(); ?>">
</head>

<body>

    <div class="container h-100">
        <div class="row h-100">
            <div class="formulario col-md-6 offset-md-3 align-self-center" id="content">

                <h3 class="text-center font-weight-bold">Ler numero entre 100 e 200</h3>
                <p class="text-center">Digite os numeros no formulário abaixo</p>

                <form action="../Config/controlador.php" method="POST" id="sendForm">

                    <input type="hidden" name="exercicio" value="exercicio-20">

                    <div class="mb-4">
                        <label for="number">Informe os numeros</label>
                        <input type="text" name="number" id="number" class="form-control" placeholder="Ex: 100, 120, 123">
                    </div>

                    <div class="mb-4">
                        <input type="submit" value="Enviar" class="btn btn-lg btn-outline-danger font-weight-bold w-100" id="btn_send">
                    </div>
                </form>

                <?php if (isset($_GET['result']) && $_GET['result'] == "error") { ?>
                    <div class="bg-warning p-3 rounded text-center result">
                        <h3 class="text-dark">OPSS...</h3>
                        <p class="text-dark"><strong>TODOS</strong> os campos devem ser preenchido!</p>
                    </div>
                <?php } ?>

                <?php if (isset($_GET['arr_number_100_200']) && ($_GET['arr_number_100_200'] == 0 || $_GET['arr_number_100_200'] == null)) { ?>
                    <div class="bg-warning p-3 rounded text-center result">
                        <h3 class="text-dark">OPSS...</h3>
                        <p class="text-dark"><strong>Nenhum</strong> número que você digitou está entre 100 e 200!</p>
                    </div>
                <?php } ?>

                <?php if (isset($_GET['result']) && $_GET['result'] == "success" && isset($_GET['arr_number_100_200']) && ($_GET['arr_number_100_200'] != 0 && $_GET['arr_number_100_200'] != null)) { ?>
                    <div class="bg-success p-3 rounded text-center result">
                        <h3 class="text-white font-weight-bold">Resultado</h3>
                        <p class="text-white">
                            <strong>Números entre 100 e 200:</strong><br>
                            <?= $_GET['arr_number_100_200']; ?>
                        </p>
                    </div>
                <?php } ?>

            </div>
        </div>
    </div>

    <!-- Scripts -->
    <script src="../assets/style/bootstrap/js/bootstrap.min.js"></script>
    <script src="../assets/js/jquery-3.6.0.min.js"></script>

    <script>
        $(document).ready(function() {
            /** ==== [INICIO]: Bloco que verifica assim que acessa a página se o campo input do form está vázio. */
            let numbers = $('#number').val();

            if (numbers == "") {
                $("#btn_send").prop('disabled', true);
            }
            /** [FIM]: ==== */

            /** ==== [INICIO]: Bloco que verifica o que está sendo digitado, mais detalhes dentro dessa função. */
            $("#sendForm").keyup(function() {
                // Variaveis
                let num = $("#number").val();
                let arr_num = num.split(','); // Converto em array informando que a quebra para o array é a Vírgula. (,)

                // Verifico se o erro está sendo apresentado e excluo
                if($("#errorForm")){
                    $("#errorForm").remove();
                }

                /**
                 * Percorrendo o array.
                 * o $.each() é comparado a foreach do PHP.
                 */
                $.each(arr_num, function(key, value){

                    // Verifico se o valor digitado é uma string, se sim o isNaN() retorna true e entra no if().
                    if(isNaN(value)){         
                        
                        // Criado uma div para apresentar o erro.
                        let returnError = $("<div>", {
                            id: "errorForm",
                            class: "bg-danger p-3 rounded text-center"
                        });

                        // Verifico se a div result está sendo mostrada. Caso sim, entra no if e exclue a mesma.
                        if($('.result')){
                            $('.result').remove();
                        }

                        // Insiro o texto de erro na div criada acima (returnError).
                        returnError.html('<h3 class="text-white">Éhhh...</h3><p class="text-white">Insira apenas <strong>Números separados por virgulas!</strong></p>');
                        // Insiro a div criada na variavel returnError dentro da div id #content
                        $("#content").append(returnError);

                        // Zero o campo input que está sendo preenchido.
                        $("#number").val("");
                        // Desabilito o btn.
                        $("#btn_send").prop("disabled", true);

                        let out;
                        out = setTimeout(alertOut, 2000);
                    } else {
                        // Habilito o btn.
                        $("#btn_send").prop("disabled", false);
                    }
                });
                
            });

            /**
             * Função usada para dar um fadeOut no elemento que apresenta o erro.
             *
             * @return void
             */
            function alertOut(){
                $("#errorForm").fadeOut(800)
            }
        });
    </script>
</body>

</html>