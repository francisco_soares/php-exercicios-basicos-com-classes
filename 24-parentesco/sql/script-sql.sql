-- Active: 1662831653079@@127.0.0.1@3306

-- CRIO A BASE DE DADOS
-- CREATE DATABASE parentesco;

-- Acesso a base de dados criada
use parentesco;

-- CRIO A TABELA FAMILIA
CREATE TABLE tb_familia(
    id_familia int not null PRIMARY KEY AUTO_INCREMENT,
    nome_familia VARCHAR(255) NOT NULL
);


INSERT INTO tb_familia(nome_familia) VALUES("Wayne");
INSERT INTO tb_familia(nome_familia) VALUES("Kent");
INSERT INTO tb_familia(nome_familia) VALUES("Allen");

-- Atualiza dado de um campo na tabela.
UPDATE tb_familia SET nome_familia = "Allen" WHERE id_familia = 3;

-- Seleciona os registro da tabela
SELECT * FROM tb_familia;

----------------------- // -----------------------

-- CRIO A TABELA PARENTES

CREATE TABLE tb_parentes(
    id_parentes INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    nome_parente VARCHAR(255),
    idade_parent INT NULL
);


-- INSERT INTO tb_parentes(nome_parente, idade_parent) VALUES("Barry", 26);
-- INSERT INTO tb_parentes(nome_parente, idade_parent) VALUES("Henry", 46);
-- INSERT INTO tb_parentes(nome_parente, idade_parent) VALUES("Nora", 42);
-- INSERT INTO tb_parentes(nome_parente, idade_parent) VALUES("Bruce", 28);
-- INSERT INTO tb_parentes(nome_parente, idade_parent) VALUES("Thomas", 50);
-- INSERT INTO tb_parentes(nome_parente, idade_parent) VALUES("Martha", 46);
-- INSERT INTO tb_parentes(nome_parente, idade_parent) VALUES("Clark", 27);
-- INSERT INTO tb_parentes(nome_parente, idade_parent) VALUES("Jonathan", 50);
-- INSERT INTO tb_parentes(nome_parente, idade_parent) VALUES("Martha", 44);

SELECT * FROM tb_parentes;
SELECT * FROM tb_familia;


----------------------- // -----------------------

-- CRIO A TABELA DE RELACIONAMENTO
CREATE TABLE bt_relaciona_parente(
    id_familia INT NOT NULL,
    id_parentes INT NOT NULL,
    FOREIGN KEY(id_familia) REFERENCES tb_familia(id_familia),
    FOREIGN KEY(id_parentes) REFERENCES tb_parentes(id_parentes)
);

-- BRUCE
INSERT INTO bt_relaciona_parente(id_familia, id_parentes) VALUES(1, 7);

-- CLARK
INSERT INTO bt_relaciona_parente(id_familia, id_parentes) VALUES(2, 9);

-- BARRY
INSERT INTO bt_relaciona_parente(id_familia, id_parentes) VALUES(3, 5);

SELECT * FROM bt_relaciona_parente;

--- CONSULTA DE RELACIONAMENTO

-- tb_familia
-- tb_parentes
SELECT p.nome_parente, f.nome_familia FROM bt_relaciona_parente AS r
LEFT JOIN tb_familia AS f ON (f.id_familia = r.id_familia)
LEFT JOIN tb_parentes AS p ON (p.id_parentes = r.id_parentes)
ORDER BY f.nome_familia DESC;