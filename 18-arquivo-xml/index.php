<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>XML - Criando e lendo</title>

    <!-- Style -->
    <link rel="stylesheet" href="../assets/style/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/style/style.css?v=<?= time(); ?>">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">

    <style>
        .nav {
            align-self: center;
            justify-content: center;
        }
    </style>

</head>

<body>

    <div class="container h-100">
        <div class="row h-100">
            <div class="formulario col-md-6 offset-md-3 align-self-center" id="content">

                <h3 class="text-center font-weight-bold">XML - Criando e lendo</h3>
                <p class="text-center">Preencha o formulário para criar o arquivo XML</p>


                <ul class="nav nav-tabs" id="nav-tab" role="tablist">
                    <li class="nav-item" role="presentation">
                        <button class="nav-link active" id="form-tab" data-bs-toggle="tab" data-bs-target="#form" type="button" role="tab" aria-controls="form" aria-selected="true">Inserir Livro</button>

                    <li class="nav-item" role="presentation">
                        <button class="nav-link" id="file-tab" data-bs-toggle="tab" data-bs-target="#file" type="button" role="tab" aria-controls="file" aria-selected="false">Livros Cadastrados</button>
                    </li>
                </ul>

                <div class="tab-content" id="myTabs">

                    <!-- ** Form content -->
                    <div class="tab-pane fade show active" id="form" role="tabpanel" aria-labelledby="form-tab">
                        <form action="../Config/controlador.php" method="POST" id="sendForm" class="mt-4">

                            <input type="hidden" name="exercicio" value="exercicio-18">

                            <div class="mb-2">
                                <label for="book_title" class="font-weight-bold">Título do Livro</label>
                                <input type="text" name="book_title" id="book_title" class="form-control" placeholder="Ex: Estudo em Vermelho">
                            </div>
                            <div class="mb-2">
                                <label for="genero_book" class="font-weight-bold">Gênero do Livro</label>
                                <input type="text" name="genero_book" id="genero_book" class="form-control" placeholder="Ex: Drama e investigação">
                            </div>
                            <div class="mb-2">
                                <label for="author_book" class="font-weight-bold">Autor do Livro</label>
                                <input type="text" name="author_book" id="author_book" class="form-control" placeholder="Ex: Sir Arthur Conan Doyle">
                            </div>
                            <div class="mb-4">
                                <label for="price_book" class="font-weight-bold">Preço</label>
                                <input type="text" name="price_book" id="price_book" class="form-control" placeholder="Ex: R$ 39.90">
                            </div>
                            <div class="mb-2">
                                <input type="submit" id="btn_send" class="btn btn-lg btn-outline-danger w-100 font-weight-bold" value="Enviar">
                            </div>
                        </form>

                        <!-- Mensagem de erro  -->
                        <?php if (isset($_GET['result']) && $_GET['result'] == "error") { ?>
                            <div class="bg-warning p-3 rounded text-center result">
                                <h3 class="text-dark">OPSS...</h3>
                                <p class="text-dark"><strong>TODOS</strong> os campos devem ser preenchido!</p>
                            </div>
                        <?php } ?>

                        <!-- Mensagem de sucesso ao cadastrar livro -->
                        <?php if (isset($_GET['result']) && $_GET['result'] == "saved_book" && isset($_GET['name_book'])) { ?>

                            <div class="bg-success p-3 rounded result">
                                <h3 class="text-white text-center">Sucesso.</h3>
                                <p class="text-white text-center">
                                    O livro <strong><?= $_GET['name_book']; ?></strong> foi cadastrado com sucesso.
                                </p>
                            </div>

                        <?php } ?>

                        <!-- Mensagem informando que o livro ja existe -->
                        <?php if (isset($_GET['result']) && $_GET['result'] == "book_exists" && isset($_GET['name_book'])) { ?>

                            <div class="bg-dark p-3 rounded result">
                                <h3 class="text-white text-center">OPSS...</h3>
                                <p class="text-white text-center">
                                    O livro <strong><?= $_GET['name_book']; ?></strong> já existe. Tente cadastrar outro
                                </p>
                            </div>

                        <?php } ?>

                        <!-- Mensagem de erro. Alguma coisa no sistema não permite inserir o livro -->
                        <?php if (isset($_GET['result']) && $_GET['result'] == "some_error" && isset($_GET['name_book'])) { ?>

                            <div class="bg-danger p-3 rounded result">
                                <h3 class="text-white text-center">OPSS...</h3>
                                <p class="text-white text-center">
                                    Erro no sistema, não foi possível cadastra o livro: <strong><?= $_GET['name_book']; ?></strong>. Por favor, tente novamente.
                                </p>
                            </div>

                        <?php } ?>
                    </div>
                    <!-- Form content ** -->

                    <!-- ** File content -->
                    <div class="tab-pane fade" id="file" role="tabpanel" aria-labelledby="file-tab">

                        <?php
                        $path = "xml/";
                        $dir = dir($path);

                        /**
                         * A função [glob()] procura por todos os caminhos que combinem com o padrão pattern de acordo com as regras usadas pela função glob() da libc, que é semelhante às regras usadas por shells comuns.
                         * Referencia: https://www.php.net/manual/pt_BR/function.glob.php
                         */

                        $dir_empty = ((count(glob("$path/*")) === 0) ? true : false);

                        /** Se o diretorio tiver arquivos, entra na condição. */
                        if ($dir_empty == false) {
                        ?>

                            <div class="bg-light p-3 rounded result mt-4 border">
                                <h4 class="text-dark text-center font-weight-bold mb-4">Livros cadastrados</h4>
                                <p class="text-dark">
                                    <?php
                                    while ($book_xml = $dir->read()) {
                                        if ($book_xml == "." || $book_xml == "..") {
                                            continue;
                                        }

                                        $book_title = str_replace('livro_', '', $book_xml);
                                        $book_title = ucfirst(str_replace('_', ' ', $book_title));
                                        $book_title = str_replace('.xml', '', $book_title);
                                        echo "<i class='bi bi-book'></i> - <a href='" . $path . $book_xml . "'>" . $book_title . "</a><br />";
                                    }
                                    ?>
                                </p>
                            </div>
                        <?php
                            $dir->close();
                        }
                        ?>
                    </div>
                    <!-- File content ** -->
                </div>

            </div>
        </div>
    </div>


    <!-- Scripts -->
    <!--  -->
    <script src="../assets/js/jquery-3.6.0.min.js"></script>
    <script src="../assets/style/bootstrap/js/bootstrap.min.js"></script>

    <script>
        $(document).ready(function() {
            let book_title = $("#book_title")
            let genero_book = $("#genero_book")
            let author_book = $("#author_book")
            let price_book = $("#price_book")

            /**
             * Faço uma validação assim que carrega toda a página.
             * Verifico se os campos #book_title, #genero_book, #author_book e #price_book está vazio.
             * Caso atenda a esse requisito, o btn de enviar é desabilitado.
             * 
             * Sendo assim, quando carrega a pagina o btn já vem desabilitado.
             */
            if (book_title.val().trim() == "" || genero_book.val().trim() == "" || author_book.val().trim() == "" || price_book.val().trim() == "") {
                $("#btn_send").prop("disabled", true)
            }

            /**
             * Aqui é feito uma validação em tempo real, assim que o usuário digita as informações.
             */
            $("#sendForm").keyup(function() {

                /**
                 * Faço uma verificação o campo e #book_title, #genero_book, #author_book e #price_book.
                 * Assim desabilito o mesmo se existir ou não.
                 */
                if ((book_title.val().trim() || !book_title.val().trim()) || (genero_book.val().trim() || !genero_book.val().trim()) || (author_book.val().trim() || !author_book.val().trim()) || (price_book.val().trim() || !price_book.val().trim())) {
                    $("#btn_send").prop("disabled", false)
                }

                /**
                 * Faço uma validação para verificar se todos os campos estão vazios.
                 * Caso atenda essa condição, desabilita o btn enviar.
                 */
                if (book_title.val().trim() == "" || genero_book.val().trim() == "" || author_book.val().trim() == "" || price_book.val().trim() == "") {
                    $("#btn_send").prop("disabled", true)
                }

                /**
                 * Valido o título do livro verificando se foi digitado espaço em branco 2 vezes, se sim, 
                 * entra na condição e retira os espaços porém preservando os valores digitados.
                 */
                if (book_title.val().indexOf("  ") > -1) {
                    $("#book_title").val(book_title.val().trim())
                }

                /**
                 * Valido o gênero do livro verificando se foi digitado espaço em branco 2 vezes, se sim, 
                 * entra na condição e retira os espaços porém preservando os valores digitados.
                 */
                if (genero_book.val().indexOf("  ") > -1) {
                    $("#genero_book").val(genero_book.val().trim())
                }

                /**
                 * Valido o autor do livro verificando se foi digitado espaço em branco 2 vezes, se sim, 
                 * entra na condição e retira os espaços porém preservando os valores digitados.
                 */
                if (author_book.val().indexOf("  ") > -1) {
                    $("#author_book").val(author_book.val().trim())
                }

                /**
                 * Valido o preço do livro verificando se foi digitado espaço em branco 2 vezes, se sim, 
                 * entra na condição e retira os espaços porém preservando os valores digitados.
                 */
                if (price_book.val().indexOf("  ") > -1) {
                    $("#price_book").val(price_book.val().trim())
                }

            })

            /**
             * Verifico se a div com o id errorForm existe, se existir, removo
             */
            if ($("#errorForm")) {
                $("#errorForm").remove()
            }

            /**
             * Crio uma nova div com ID(errorForm)
             */
            var returnError = $("<div>", {
                id: "errorForm",
                class: "bg-danger p-3 rounded text-center"
            })

            /**
             * Atribuo o texto ao html a nova div errorForm
             */
            returnError.html('<h3 class="text-white">Éhhh...</h3><p class="text-white">Todos os campos são obrigatórios. <strong>Digite algo!</strong></p>')

            /**
             * valido os elementos e passoa a ação de blur.
             * Se algum dos campos perder o foco, o mesmo vai verificar na condição do IF qual campo tem o valor menor que 3.
             */
            $("#book_title, #genero_book, #author_book, #price_book").blur(function() {
                if (book_title.val().length < 3 || genero_book.val().length < 3 || author_book.val().length < 3 || price_book.val().length < 3) {
                    /**
                     * Insero a nova div errorForm ao DOM
                     */
                    $("#content").append(returnError)
                } else {

                    $("#errorForm").remove()

                }
            })
        })
    </script>
</body>

</html>