<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gerar relatório de usuário</title>

    <!-- Style -->
    <link rel="stylesheet" href="../assets/style/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/style/style.css?v=<?= time(); ?>">
</head>

<body>

    <div class="container h-100">
        <div class="row h-100">
            <div class="formulario col-md-6 offset-md-3 align-self-center" id="conteudo">
                <h3 class="font-weight-bold text-uppercase text-center">Gerar relatório de usuário</h3>
                <p class="mb-5 text-center">Ao clicar será gerado o arquivo excel para download.</p>

                <form action="../Config/controlador.php" method="post" id="sendForm">

                    <input type="hidden" name="exercicio" value="exercicio-27">
                    <div class="mb-4">
                        <input type="submit" value="Gerar arquivo" id="btn_enviar" class="btn btn-lg btn-outline-danger w-100 font-weight-bold">
                    </div>
                </form>

                <?php if (isset($_GET['result']) && $_GET['result'] == 'success' && file_exists('person.xlsx')): ?>
                    <div class="bg-light pt-3 pb-3 pr-5 pl-5 rounded result">
                        <h3 class="text-center font-weight-bold">Arquivo gerado</h3>
                        <p>Caso não tenha feito o download, <a href="../27-excel/person.xlsx" download="">clique aqui</a>.</p>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <!-- Scripts -->
    <script src="../assets/style/bootstrap/js/bootstrap.min.js"></script>
    <script src="../assets/js/jquery-3.6.0.min.js"></script>

    <script>
        $(document).ready(function() {
            var file = <?php echo file_exists('person.xlsx') ? 'true' : 'false'; ?>;
            if (file == true) {
                $('#btn_enviar').attr('disabled', 'disabled');
            }
        });
    </script>

</body>

</html>