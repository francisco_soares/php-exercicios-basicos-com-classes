<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Peso Ideal</title>

    <!-- Style -->
    <link rel="stylesheet" href="../assets/style/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/style/style.css?v=<?= time(); ?>">
</head>

<body>

    <div class="container h-100">
        <div class="row h-100">
            <div class="formulario col-md-6 offset-md-3 align-self-center" id="conteudo">
                <h3 class="font-weight-bold text-uppercase text-center">Calculo do Peso Ideal</h3>
                <p class="mb-4 text-center">Informe no formulário seu sexo e sua altura</p>

                <form action="../Config/controlador.php" method="POST" id="enviarForm">
                    <input type="hidden" name="exercicio" value="exercicio-10">

                    <p class="font-weight-bold">Sexo:</p>
                    <div class="form-check">
                        <input type="radio" name="sexo" value="M" id="sexo_masculino" class="form-check-input">
                        <label for="sexo_masculino" class="form-check-label">Masculino</label>
                    </div>

                    <div class="mb-4 form-check">
                        <input type="radio" name="sexo" value="F" id="sexo_feminino" class="form-check-input">
                        <label for="sexo_feminino" class="form-check-label">Feminino</label>
                    </div>
                    <div class="mb-4">
                        <label for="altura" class="font-weight-bold">Altura:</label>
                        <input type="text" class="form-control" name="altura" id="altura" placeholder="Ex: 1.75">
                    </div>

                    <div class="mb-4">
                        <input type="submit" value="Enviar" class="btn btn-lg btn-outline-danger w-100 font-weight-bold text-uppercase">
                    </div>
                </form>

                <?php if (isset($_GET['result']) && $_GET['result'] == 'error') { ?>
                    <div class="bg-warning p-3 rounded text-center result">
                        <h3 class="text-dark">OPSS...</h3>
                        <p class="text-dark"><strong>TODOS</strong> os campos devem ser preenchido!</p>
                    </div>
                <?php } ?>
                <?php if (isset($_GET['result']) && $_GET['result'] == 'maior') { ?>
                    <div class="bg-warning p-3 rounded text-center result">
                        <h3 class="text-dark">OPSS...</h3>
                        <p class="text-dark">Informe sua altura correta.<br> <strong>Exemplo: </strong> 1.75</p>
                    </div>
                <?php } ?>
                <?php if (isset($_GET['sexo']) && isset($_GET['altura'])) { ?>
                    <div class="bg-success p-3 rounded  result">
                        <h3 class="text-white text-center">Resultado</h3>
                        <p class="text-white">
                            <strong>Sua altura: </strong> <?= $_GET['altura']; ?><br>
                            <strong>Seu sexo: </strong> <?= ($_GET['sexo'] == "F") ? 'Feminino' : 'Masculino'; ?><br>
                            <strong>Peso ideal: </strong> <?= $_GET['result']; ?>
                        </p>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>

    <!-- Scripts -->
    <script src="../assets/style/bootstrap/js/bootstrap.min.js"></script>
    <script src="../assets/js/jquery-3.6.0.min.js"></script>

    <script>
        $('#enviarForm').submit(function(e) {

            // Variaveis
            let altura = parseFloat($('#altura').val())
            let sexo = $("input[name='sexo']:checked").val()
            let out

            if ($('#errorForm')) {
                $('#errorForm').remove()
            }

            if (isNaN(altura) || sexo == undefined) {
                e.preventDefault()

                let returnError = $('<div>', {
                    id: 'errorForm',
                    class: 'bg-danger p-3 rounded text-center'
                })

                returnError.html('<h3 class="text-white">Éhhh...</h3><p class="text-white">Insira apenas <strong>Números</strong>!</p>')

                $("#conteudo").append(returnError)

                if ($('.result')) {
                    $('.result').remove()
                }
            }

            out = setTimeout(alertOut, 2000)
        })

        function alertOut() {
            $('#errorForm').fadeOut(400)
        }
    </script>
</body>

</html>