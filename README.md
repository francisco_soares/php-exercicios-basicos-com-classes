# PHP - Exercícios básicos com classes
Aqui você vai encontrar vários exercícios utilizando da premissa do MVC(Model, View, Controller).<br>
São exercício básicos para melhorar suas habilidades em programação.
<br>

 ### Pré-requisitos

- PHP >= 8.1
- Apache >= 2.4.52
- MySQL >= 8.0.40
- Javascript
- jQuery (Ajax)

### Configuração
- O projeto foi configurado no Pop!_OS 22.04 LTS(Linux).
- Na configuração, não é utilizado o XAMPP ou qualquer outro programa similar. Para esse caso, foi configurado direto no Apache passando todos os caminhos devidos dentros dos arquivos de configuração do mesmo.
- O mesmo acontece para o MySQL.


#### IMPORTANTE
Não esqueça de iniciar o Apache e o MySQL
```
apache_start
```

```
mysql_start
```