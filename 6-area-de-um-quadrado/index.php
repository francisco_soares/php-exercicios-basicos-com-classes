<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Área de quadro</title>

    <!-- Style -->
    <link rel="stylesheet" href="../assets/style/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/style/style.css?v=<?= time(); ?>">
</head>

<body>

    <div class="container h-100">
        <div class="row h-100">
            <div class="formulario col-md-6 offset-md-3 align-self-center text-center" id="conteudo">
                <h3 class="text-dark font-weight-bold text-uppercase">Área do quadrado</h3>
                <p class="text-dark mb-5">Informe abaixo a área do quadrado</p>

                <form action="../Config/controlador.php" method="POST" id="enviarForm">
                    <input type="hidden" name="exercicio" value="exercicio-6">

                    <div class="mb-4">
                        <label for="num" class="font-weight-bold">Informe um número</label>
                        <input type="text" name="num" id="num" class="form-control form-control-lg" placeholder="Ex: 10">
                    </div>

                    <div class="mb-4">
                        <input type="submit" value="Enviar" class="btn btn-lg btn-outline-danger w-100 font-weight-bold text-uppercase">
                    </div>
                </form>

                <?php if (isset($_GET['result']) && $_GET['result'] == 'error') { ?>
                    <div class="bg-warning p-3 rounded text-center result">
                        <h3 class="text-dark">OPSS...</h3>
                        <p class="text-dark"><strong>TODOS</strong> os campos devem ser preenchido!</p>
                    </div>
                <?php } elseif (isset($_GET['area']) && $_GET['area']) { ?>
                    <div class="bg-success p-3 rounded text-center result">
                        <h3 class="text-white">Resultado</h3>
                        <p class="text-white">
                            <strong>Área: </strong> <?= $_GET['area']; ?>
                        </p>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>

    <!-- Scripts -->
    <script src="../assets/style/bootstrap/js/bootstrap.min.js"></script>
    <script src="../assets/js/jquery-3.6.0.min.js"></script>

    <script>
        $("#enviarForm").submit(function(e) {

            // Variavel
            let num = parseFloat($('#num').val())
            let out

            /* Entro na condição para verificar se o numero informado é isNaN(Not a Number) */
            if (isNaN(num)) {
                // Paro a ação do submit do formulário.
                e.preventDefault()

                // Verifico se a div que mostra o erro está criada no DOM. Se estiver, remove.
                if ($('#errorForm')) {
                    $('#errorForm').remove()
                }

                // Crio a div para apresentar o erro
                let returnError = $('<div>', {
                    id: 'errorForm',
                    class: 'bg-danger p-3 rounded text-center'
                })

                // Insiro o texto de erro.
                returnError.html('<h3 class="text-white">Éhhh...</h3><p class="text-white">Insira apenas <strong>Números</strong>!</p>')

                // Insiro a div de erro no DOM.
                $("#conteudo").append(returnError)

                if ($('.result')) {
                    $('.result').remove()
                }

                // Crio um delay para a div de erro sair da tela.
                out = setTimeout(alertOut, 3000)
            }
        })

        function alertOut() {
            $('#errorForm').fadeOut(700)
        }
    </script>
</body>

</html>