<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Divide por 10, 5 e 2</title>

    <!-- Style -->
    <link rel="stylesheet" href="../assets/style/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/style/style.css?v=<?= time(); ?>">
</head>

<body>

    <div class="container h-100">
        <div class="row h-100">
            <div class="formulario col-md-6 offset-md-3 align-self-center" id="content">
                <h3 class="text-center font-weight-bold">Identificar o mês</h3>
                <p class="text-center">Informe um número no formulário e enviei para saber o mesmo correspondente</p>

                <form action="../Config/controlador.php" method="POST" id="sendForm">
                    <input type="hidden" name="exercicio" value="exercicio-15">

                    <div class="mb-4">
                        <label for="number" class="font-weight-bold">Informe um número</label>
                        <input type="text" name="number" id="number" class="form-control" placeholder="Ex: 1">
                    </div>

                    <div class="mb-4">
                        <input type="submit" value="Enviar" id="btn_send" class="btn btn-lg btn-outline-danger w-100 font-weight-bold">
                    </div>
                </form>

                <?php if (isset($_GET['result']) && $_GET['result'] == "error") { ?>
                    <div class="bg-warning p-3 rounded text-center result">
                        <h3 class="text-dark">OPSS...</h3>
                        <p class="text-dark"><strong>TODOS</strong> os campos devem ser preenchido!</p>
                    </div>
                <?php } ?>

                <?php if (isset($_GET['month']) && $_GET['month'] != "n") { ?>
                    <div class="bg-success p-3 rounded text-center result">
                        <h3 class="text-white">Resultado</h3>
                        <p class="text-white">
                            O mês escolhido foi: <strong><?= $_GET['month']; ?></strong>
                        </p>
                    </div>
                <?php } else if(isset($_GET['month']) && $_GET['month'] == "n") { ?>
                    <div class="bg-danger p-3 rounded text-center result">
                        <h3 class="text-white">Resultado</h3>
                        <p class="text-white">
                            O mês <strong>Não Existe</strong>
                        </p>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>


    <!-- Scripts -->
    <script src="../assets/style/bootstrap/js/bootstrap.min.js"></script>
    <script src="../assets/js/jquery-3.6.0.min.js"></script>
    <script>
        $(document).ready(function() {
            let number = $('#number').val()

            if (number == "") {
                $('#btn_send').prop('disabled', true)
            }
        })

        $('#sendForm').keyup(function() {
            let num = $('#number').val()

            if (num || !num) {
                $("#btn_send").prop("disabled", false)
            }

            if (num == "") {
                $("#btn_send").prop("disabled", true)
            }

            if ($('#errorForm')) {
                $('#errorForm').remove()
            }

            if (isNaN(num)) {
                $("#btn_send").prop("disabled", true)

                let returnError = $('<div>', {
                    id: "errorForm",
                    class: "bg-danger p-3 rounded text-center"
                })

                if ($('.result')) {
                    $('.result').remove()
                }

                returnError.html('<h3 class="text-white">Éhhh...</h3><p class="text-white">Insira apenas <strong>Números</strong>!</p>')

                $('#content').append(returnError)
            }
        })
    </script>
</body>

</html>