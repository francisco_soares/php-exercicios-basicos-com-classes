<?php

class Video
{
    // Atributos privados
    private $id_video;
    private $titulo_video;
    private $url_video;
    private $data_video;

    /**
     * O parametro $attr retorna o valor que é passado no metodo mágico __set.
     * Mais especifico no $this->$attr = $value;
     * 
     * Dessa forma, quando chamado o método mágico __get(), o parametro passado é o mesmo nome dos atributos privados.
     * Ex: $this->video->__get('titulo_video').
     * No exemplo, estou informando para pegar o valor do atributo $titulo_video que é retornado no método __get()
     *
     * @param string $attr
     * @return void
     */
    public function __get($attr)
    {
        return $this->$attr;
    }

    /**
     * Método mágico que recebe o valor via parametro juntamente com o atributo
     * Ex: $Video->__set('titulo_video', $titulo_video)
     *
     * @param string $attr
     * @param string $value
     */
    public function __set($attr, $value)
    {
        $this->$attr = $value;
    }
}
