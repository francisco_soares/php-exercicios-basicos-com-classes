<?php
/**
 * Classe simples de conexão
 */
class Conexao
{
    // Atributos da classe para conexao
    private $host   = "localhost";
    private $dbname = "exercicio_video";
    private $user   = "root";
    private $pass   = "";

    /**
     * Método de conexão ao bando de dados
     *
     * @return $conexao
     */
    public function conectar()
    {
        try {
            // Faço a conexão simples com PDO
            // Sintax:
            // PDO("<driver-do-SGBD>:hots=<name-host>;fbname=<name-database>;", "<user>", "<password>")
            // PDO("mysql:host=localhost;dbname=exercicio_video;", "root", "")
            $conexao = new PDO("mysql:host={$this->host};dbname={$this->dbname};", "{$this->user}", "{$this->pass}");
            
            // Retorno a conexão
            return $conexao;
        } catch (PDOException $e) {
            // Faço tratamento de erro com PDOException
            // Métodos do PDOException
            // getMenssage() | getFile | getLine | getCode
            // Forma chamar:
            // $e->getMessage() | $e->getFile() | $e->getLine | $e->getCode
            echo "
            <div style='color: #FEEAEA; background: #d12121; border:1px solid #7C1515; padding: 10px; font-family: Calibri, sans-serif; border-radius: 5px;'>
                <strong>Erro:</strong> {$e->getMessage()} |
                <strong>Arquivo:</strong> {$e->getFile()} |
                <strong>Linha:</strong> {$e->getLine()} |
                <strong>Código:</strong> {$e->getCode()}
            </div>    
            ";
        }
    }
}
