<?php

class VideoTarefas
{
    // Atributos
    private $conexao;
    private $video;
    private $result;

    /**
     * Método construtor que recebe dois parametros do tipo Objeto
     *
     * @param Conexao $conexao
     * @param Video $video
     */
    public function __construct(Conexao $conexao, Video $video)
    {
        $this->conexao = $conexao->conectar();
        $this->video = $video;
    }

    public function insert()
    {
        $queryInsert = "INSERT INTO videos(titulo_video, url_video) VALUES(:titulo_video, :url_video)";
        $prepareInsert = $this->conexao->prepare($queryInsert);
        $prepareInsert->bindValue(':titulo_video', $this->video->__get('titulo_video'));
        $prepareInsert->bindValue(':url_video', $this->video->__get('url_video'));
        $prepareInsert->execute();
        $this->result = $this->conexao->lastInsertId();
    }

    public function readAll(){
        $querySelect = "SELECT id_video, titulo_video, url_video FROM videos";
        $prepareSelect = $this->conexao->prepare($querySelect);
        $prepareSelect->execute();
        $this->result = $prepareSelect->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getResult(){
        return $this->result;
    }
}
