<?php
// Faço os requires
require "Video.class.php";
require "Conexao.class.php";
require "VideoTarefas.class.php";

// Verifico a ação sendo passado via POST no formulário.
$acao = isset($_POST['acao']) ? $_POST['acao'] : $acao;

// Instancio/Crio o objeto a classe Conexão
$Conexao = new Conexao;
// Instancio/Crio o objeto a classe Vídeo
$Video = new Video();

if ($acao === "inserir") {

    // Recupero os campos passado no formulário
    $titulo_video = $_POST['titulo_video'] ? $_POST['titulo_video'] : '';
    $url_video = $_POST['url_video'] ? $_POST['url_video'] : '';

    // Faço uma condição para verificar se os campos passado no formulário não está vázio.
    if (!empty($titulo_video) && !empty($url_video)) {

        // Seto os valores no método mágico __set()
        $Video->__set('titulo_video', $titulo_video);
        $Video->__set('url_video', $url_video);

        // Instancio/Crio o objeto a classe VideoTarefas e passo dois parametros no formato OBJ.
        $videoTarefa = new VideoTarefas($Conexao, $Video);
        $videoTarefa->insert();

        // Faço uma condição verificando se foi inserido o dado, caso true entra na primeira condição
        if ($videoTarefa->getResult()) {
            header("Location: ver-video.php");
        } else {
            header("Location: index.php?acao=erro");
        }
    } else {
        header("Location: index.php?erroCampo=erro");
    }
} elseif ($acao === "recuperar") {
    $videoTarefa = new VideoTarefas($Conexao, $Video);
    $videoTarefa->readAll();    
}
