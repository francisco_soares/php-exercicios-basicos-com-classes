<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Envia vídeos</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../assets/style/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/style/style.css?v=<?= time(); ?>">
</head>

<body>

    <div class="container h-100">
        <div class="row h-100">
            <div class="formulario col-lg-6 offset-lg-3 align-self-center">
                <h3 class="mb-5 text-center font-weight-bold text-uppercase">Envie seu curso</h3>

                <form action="video_controller.php" id="formulario" method="POST">
                    <input type="hidden" name="acao" value="inserir">
                    <div class="mb-3">
                        <label class="font-weight-bold">Título do vídeo</label>
                        <input type="text" name="titulo_video" id="titulo_video" placeholder="Insira o título do vídeo aqui." class="form-control form-control-lg">
                    </div>

                    <div class="mb-3">
                        <label class="font-weight-bold">URL do vídeo</label>
                        <input type="text" name="url_video" id="url_video" placeholder="https://youtube.com.br/meu-video" class="form-control form-control-lg">
                    </div>

                    <div class="mb-3">
                        <input type="submit" value="Enviar" class="btn btn-outline-danger btn-lg w-100 text-uppercase font-weight-bold">
                    </div>
                </form>

                <?php
                if (isset($_GET['erroCampo']) && !empty($_GET['erroCampo']) && $_GET['erroCampo'] == 'erro') {
                ?>
                    <div class="alert alert-danger text-center"><strong>---- ERRO ----<br></strong> Opsss... Você precisa inserir o <strong>Título</strong> e <strong>URL</strong> do vídeo.</div>
                <?php } ?>

            </div>
        </div>
    </div>

    <!-- Bootstrap JS -->
    <script src="../assets/style/bootstrap/js/bootstrap.min.js"></script>
    <script src="../assets/js/jquery-3.6.0.min.js"></script>
</body>

</html>