<?php
$acao = "recuperar";
require('config/Models/VideoController.php');

?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ver vídeos</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../assets/style/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/style/style.css?v=<?= time(); ?>">
</head>

<body>

    <div class="container">
        <div class="row">
            <?php if ($videoTarefa->getResult()) { ?>
                <?php foreach ($videoTarefa->getResult() as $video) { ?>
                    <?php
                    $position = strpos($video['url_video'], '=');
                    $result = substr($video['url_video'], $position);
                    $result = str_replace('=', '', $result);
                    ?>
                    <div class="col-sm-3 p-2 d-flex align-items-stretch">
                        <div class="bg-white rounded text-center pb-3">
                            <img src="https://img.youtube.com/vi/<?= $result; ?>/0.jpg" class="img-fluid rounded-top mb-3" alt="">
                            <h5><?= $video['titulo_video']; ?></h5>
                            <button type="button" id="<?= $video['id_video'] ?>" class="btn btn-primary mostraModal" data-toogle="moda" data-target="#modalVideo" onclick="mostraVideo('<?= $result ?>')">Assistir agora</button>
                        </div>
                    </div>
                <?php } ?>
            <?php } ?>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalVideo" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="embed-responsive embed-responsive-16by9" id="embed_video">
                                    <!-- <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/zpOULjyy-n8?rel=0" allowfullscreen></iframe> -->
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" id="close_modal" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <!-- Bootstrap JS -->
    <script src="../assets/js/jquery-3.6.0.min.js"></script>
    <script src="../assets/style/bootstrap/js/bootstrap.min.js"></script>


    <script>
        function mostraVideo(url_video) {
            $('.mostraModal').on('click', function() {
                $('#embed_video').append('<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/' + url_video + '?rel=0" allowfullscreen></iframe>')

                $('#modalVideo').modal('show')
            })
        }

        $('#close_modal').on('click', function() {
            $('#embed_video').empty()
        })
        // $('#modalVideo').modal('show')
    </script>
</body>

</html>