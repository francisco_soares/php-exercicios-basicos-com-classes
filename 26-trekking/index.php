<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>João - O Pescador</title>

    <!-- Style -->
    <link rel="stylesheet" href="../assets/style/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/style/style.css?v=<?= time(); ?>">
</head>

<body>

    <div class="container h-100">
        <div class="row h-100">
            <div class="formulario col-md-6 offset-md-3 align-self-center" id="conteudo">
                <h3 class="font-weight-bold text-uppercase text-center">João - O Pescador</h3>
                <p class="mb-5 text-center">Calculador de excesso de peso de peixe(s)</p>

                <form action="../Config/controlador.php" method="post" id="sendForm">

                    <input type="hidden" name="exercicio" value="exercicio-11">

                    <div class="mb-4">
                        <label for="peso" class="font-weight-bold">Informe o peso do peixe</label>
                        <input type="text" name="peso" id="peso" class="form-control" placeholder="Ex: 15 ou 15.5">
                    </div>
                    <div class="mb-4">
                        <label for="peso_excedido" class="font-weight-bold">Peso excedido</label>
                        <input type="text" name="peso_excedido" id="peso_excedido" class="form-control" disabled>
                    </div>
                    <div class="mb-4">
                        <label for="valor_multa_por_kilo" class="font-weight-bold">Valor da multa por kilo excedido</label>
                        <input type="text" name="valor_multa_por_kilo" id="valor_multa_por_kilo" class="form-control" disabled>
                    </div>
                    <div class="mb-4">
                        <input type="submit" value="Enviar" id="btn_enviar" class="btn btn-lg btn-outline-danger w-100 font-weight-bold">
                    </div>
                </form>

                <?php if (isset($_GET['result']) && $_GET['result'] == "error") { ?>
                    <div class="bg-warning p-3 rounded text-center result">
                        <h3 class="text-dark">OPSS...</h3>
                        <p class="text-dark"><strong>TODOS</strong> os campos devem ser preenchido ou não pode ser <strong>0</strong> (zero)!</p>
                    </div>
                <?php } ?>

                <?php if ((isset($_GET['weight']) && !empty($_GET['weight'])) && (isset($_GET['weightExceeded']) && !empty($_GET['weightExceeded'])) && (isset($_GET['fineAmount']) && !empty($_GET['fineAmount']))) { ?>
                    <div class="bg-success pt-3 pb-3 pr-5 pl-5 rounded  result">
                        <h3 class="text-white text-center font-weight-bold">Resultado</h3>
                        <p class="text-white">
                            <strong>Peso: </strong> <?= $_GET['weight']; ?> kg<br>
                            <strong>Peso excedido: </strong> <?= $_GET['weightExceeded']; ?> kg<br>
                            <strong>Multa de peso excedido: </strong> R$ <?= $_GET['fineAmount']; ?>
                        </p>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>


    <!-- Scripts -->
    <script src="../assets/style/bootstrap/js/bootstrap.min.js"></script>
    <script src="../assets/js/jquery-3.6.0.min.js"></script>

    <script>
        $("#peso").keyup(function() {

            let peso = $('#peso').val()
            if (peso || !peso) {
                $("#btn_enviar").prop("disabled", false)
            }

            let peso_float = parseFloat(peso)
            let peso_excedido

            if (peso_float > 50) {
                peso_excedido = peso_float - 50
                $("#peso_excedido").val(peso_excedido + " kg")
                $("#valor_multa_por_kilo").val("R$ " + peso_excedido * 4)
            } else {
                $("#peso_excedido").val('')
                $("#valor_multa_por_kilo").val('')
            }

            if($(".result")){
                $(".result").remove()
            }

            if ($('#errorForm')) {
                $('#errorForm').remove()
            }

            console.log(isNaN(peso))

            if (isNaN(peso)) {

                $("#btn_enviar").prop("disabled", true)

                let returnError = $('<div>', {
                    id: 'errorForm',
                    class: 'bg-danger p-3 rounded text-center'
                })

                returnError.html('<h3 class="text-white">Éhhh...</h3><p class="text-white">Insira apenas <strong>Números</strong>!</p>')

                $('#conteudo').append(returnError)
            }
        })
    </script>

</body>

</html>