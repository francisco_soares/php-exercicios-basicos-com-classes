<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Enviando CSV</title>

    <!-- Style -->
    <link rel="stylesheet" href="../assets/style/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/style/style.css?v=<?= time(); ?>">
</head>

<body>

    <div class="container-loading">
        <div class="modal-container">
            <div class="spinner"></div>
            <p>Enviando...</p>
        </div>
    </div>

    <div class="container h-100">
        <div class="row h-100">
            <div class="formulario col-md-6 offset-md-3 align-self-center" id="content">
                <h3 class="font-weight-bold text-uppercase text-center">UFs - BRASIL</h3>
                <p class="mb-5 text-center">Envio de arquivo com os UFs do Brasil.</p>

                <form method="post" id="sendForm" enctype="multipart/form-data">

                    <input type="hidden" name="exercicio" value="trekking">

                    <div class="mb-4">
                        <label for="peso" class="font-weight-bold">Insira o arquivo .CSV</label>
                        <div class="custom-file">
                            <input type="file" name="abbr" id="abbr" class="custom-file-input" aria-describedby="abbr">
                            <label class="custom-file-label" id="label_input_file" for="abbr">Enviar arquivo</label>
                        </div>

                    </div>
                    <div class="mb-4">
                        <input type="submit" value="Enviar" id="btn_send" class="btn btn-lg btn-outline-danger w-100 font-weight-bold">
                    </div>
                </form>

                <?php if (isset($_GET['result']) && $_GET['result'] == "error") { ?>
                    <div class="bg-warning p-3 rounded text-center result">
                        <h3 class="text-dark">OPSS...</h3>
                        <p class="text-dark"><strong>TODOS</strong> os campos devem ser preenchido ou não pode ser <strong>0</strong> (zero)!</p>
                    </div>
                <?php } ?>

                <?php if ((isset($_GET['weight']) && !empty($_GET['weight'])) && (isset($_GET['weightExceeded']) && !empty($_GET['weightExceeded'])) && (isset($_GET['fineAmount']) && !empty($_GET['fineAmount']))) { ?>
                    <div class="bg-success pt-3 pb-3 pr-5 pl-5 rounded  result">
                        <h3 class="text-white text-center font-weight-bold">Resultado</h3>
                        <p class="text-white">
                            <strong>Peso: </strong> <?= $_GET['weight']; ?> kg<br>
                            <strong>Peso excedido: </strong> <?= $_GET['weightExceeded']; ?> kg<br>
                            <strong>Multa de peso excedido: </strong> R$ <?= $_GET['fineAmount']; ?>
                        </p>
                    </div>
                <?php } ?>

                <?php if (isset($_GET['result']) && $_GET['result'] == "errorFormat") { ?>
                    <div class="bg-warning p-3 rounded text-center result">
                        <h3 class="text-dark font-weight-bold">OPSS...</h3>
                        <p class="text-dark"><strong>FORMATO</strong> não aceito, por favor envie uma imagem em JPG, PNG ou GIF!</p>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>


    <!-- Scripts -->
    <script src="../assets/style/bootstrap/js/bootstrap.min.js"></script>
    <script src="../assets/js/jquery-3.6.0.min.js"></script>

    <script>
        $(document).ready(function() {

            let abbr = $("#abbr").val();

            if (abbr == "") {
                $("#btn_send").prop("disabled", true);
            }

            $("#abbr").change(function(e) {
                let abbr_keyup = e.target.files[0].name;
                let fileType = e.target.files[0].type;

                let arrFile = [
                    "text/csv",
                ];

                // Verifico se a DIV ID errorForm está sendo apresentada e a removo
                if ($("#errorForm")) {
                    $("errorForm").remove();
                }

                if ($(".result")) {
                    $(".result").remove();
                }

                /* Verifico se o tipo do arquivo não existe, se não existir, entra no if() */
                if ($.inArray(fileType, arrFile) == -1) {

                    // Crio a DIV ID errorForm
                    let errorFile = $("<div>", {
                        id: "errorForm",
                        class: "bg-danger p-3 rounded text-center"
                    });

                    // Desabilito o botão de enviar
                    $("#btn_send").prop("disabled", true);

                    // Insiro um texto na DIV ID errorForm
                    errorFile.html('<h3 class="text-white">Éhhh...</h3><p class="text-white">OPSSS... Formato do arquivo incorreto, favor enviar arquivo em <strong>CSV</strong></p>');

                    // Mostro a DIV ID errorForm ao final mas dentro da DIV ID content
                    $("#content").append(errorFile);

                    // Limpo o campo input do CV
                    $("#cv").val('');

                    /* Se o formato for diferente de PDF e DOC, eu zero o campo */
                    $("#label_input_file").html("Enviar arquivo");
                    /** Continuar a validação quando é enviado um arquivo diferente de PDF e DOC */
                } else {

                    if ($("#errorForm")) {
                        $("#errorForm").remove();
                    }

                    /* No momento que subo a imagem, é pego no nome da mesma e altera o campo #label_input_file*/
                    $("#label_input_file").html(abbr_keyup);

                    $("#btn_send").prop("disabled", false);
                }
            });

            $("#sendForm").on("submit", function(e) {
                e.preventDefault();
                let formData = new FormData(this);


                // ../Config/controlador.php
                $.ajax({
                    url: '../Config/controlador.php',
                    type: 'POST',
                    data: formData,
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    beforeSend: function() {
                        $('.container-loading').fadeIn(500).css({
                            'display': 'flex'
                        });
                    },
                    success: function(data, textStatus, jqXHR) {
                        $('.container-loading').fadeOut(500).css({
                            'display': 'none'
                        });

                        if ($("#errorForm")) {
                            $("#errorForm").remove();
                        }

                        if ($("#successForm")) {
                            $("#successForm").remove();
                        }

                        if (data.status == false) {
                            // Crio a DIV ID errorForm
                            let errorFile = $("<div>", {
                                id: "errorForm",
                                class: "bg-warning p-3 rounded text-center"
                            });

                            // Desabilito o botão de enviar
                            $("#btn_send").prop("disabled", true);

                            // Insiro um texto na DIV ID errorForm
                            errorFile.html('<h5 class="text-dark font-weight-bold">OPSS...</h4><p class="text-dark"><strong>FORMATO</strong> não aceito, por favor envie <strong>CSV</strong></p>');

                            // Mostro a DIV ID errorForm ao final mas dentro da DIV ID content
                            $("#content").append(errorFile);
                        } else {
                            let successFile = $("<div>", {
                                id: "successForm",
                                class: "bg-success p-3 rounded text-center"
                            });

                            // Desabilito o botão de enviar
                            $("#btn_send").prop("disabled", true);

                            // Insiro um texto na DIV ID errorForm
                            successFile.html('<h5 class="text-white font-weight-bold">SHOW</h4><p class="text-white">Dados inseridos com sucesso</p>');

                            // Mostro a DIV ID errorForm ao final mas dentro da DIV ID content
                            $("#content").append(successFile);
                        }
                    }
                });
            });

        });
    </script>

</body>

</html>