<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Impressão de palavra</title>

    <!-- Style -->
    <link rel="stylesheet" href="../assets/style/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/style/style.css?v=<?= time(); ?>">
</head>

<body>

    <div class="container h-100">
        <div class="row h-100">
            <div class="formulario col-6-md offset-md-3 align-self-center" id="content">
                <h3 class="text-center font-weight-bold">Impressão de Palavra</h3>
                <p class="text-center">Digite apenas uma palavra e veja como será impressa na tela.</p>

                <form action="../Config/controlador.php" method="POST" id="sendForm">

                    <input type="hidden" name="exercicio" value="exercicio-17">

                    <div class="mb-4">
                        <label for="word" class="font-font-weight-bold">Informe uma palavra</label>
                        <input type="text" name="word" id="word" class="form-control" placeholder="Ex: Trilha">
                    </div>

                    <div class="mb-4">
                        <input type="submit" value="Enviar" class="btn btn-lg btn-outline-danger font-weight-bold w-100" id="btn_send">
                    </div>
                </form>

                <?php if (isset($_GET['result']) && $_GET['result'] == "error") { ?>
                    <div class="bg-warning p-3 rounded text-center result">
                        <h3 class="text-dark">OPSS...</h3>
                        <p class="text-dark"><strong>TODOS</strong> os campos devem ser preenchido!</p>
                    </div>
                <?php } ?>

                <?php if (isset($_GET['word']) && $_GET['word'] != "") { ?>
                    <div class="bg-success p-3 rounded result">
                        <h3 class="text-white text-center">Resultado</h3>
                        <p class="text-white">
                            <?php
                            /* IMPRESSAO */
                            for ($i = 1; $i <= 1; $i++) {
                                echo $_GET['word'];
                                if ($i == 1) {
                                    echo "<br>";

                                    /* IMPRESSAO IMPRESSAO */
                                    for ($j = 1; $j <= 2; $j++) {
                                        echo $_GET['word'] . " ";
                                        if ($j == 2) {
                                            echo "<br>";

                                            /* IMPRESSAO IMPRESSAO IMPRESSAO */
                                            for ($x = 1; $x <= 3; $x++) {
                                                echo $_GET['word'] . " ";
                                                if ($x == 3) {
                                                    echo "<br>";

                                                    /* IMPRESSAO IMPRESSAO IMPRESSAO IMPRESSAO */
                                                    for ($z = 1; $z <= 4; $z++) {
                                                        echo $_GET['word'] . " ";
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            ?>
                        </p>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <!-- Scripts -->
    <script src="../assets/style/bootstrap/js/bootstrap.min.js"></script>
    <script src="../assets/js/jquery-3.6.0.min.js"></script>

    <script>
        /**
         * Faço uma validação assim que carrega toda a página.
         * Verifico se os campos #word está vazio.
         * Caso atenda a esse requisito, o btn de enviar é desabilitado.
         * 
         * Sendo assim, quando carrega a pagina o btn já vem desabilitado.
         */
        $(document).ready(function() {

            // Pego os valores dos campos
            let word = $('#word').val().trim()

            if (word == "") {
                $("#btn_send").prop('disabled', true)
            }
        })

        $("#sendForm").keyup(function() {

            // Pego os valores dos campos
            let word = $("#word").val()

            /**
             * Faço uma verificação o campo e word.
             * Assim desabilito o mesmo se existir ou não.
             */
            if (word || !word) {
                $("#btn_send").prop("disabled", false)
            }

            /**
             * Faço uma validação para verificar se o campo está vazio.
             * Caso atenda essa condição, desabilita o btn enviar.
             */
            if (word == "") {
                $("#btn_send").prop("disabled", true)
            }

            /**
             * Verifico dentro do campo #word se apresenta espaços em branco acima de 1, caso atenda, o mesmo retira todos os espaços em branco.
             * Assim, deixando apenas o que você digitou.
             */
            if (word.indexOf("  ") > -1) {
                $("#word").val(word.trim())
            }

            if ($("#errorForm")) {
                $("#errorForm").remove()
            }

            /**
             * Verifico se tem mais de um espaço em branco em seguida.
             * Assim desabilita o btn enviar.
             * 
             * Também é mostrado o erro
             */
            if (word.indexOf("  ") == 0) {

                /** Desabilito o botão de enviar */
                $("#btn_send").prop("disabled", true)

                /**
                 * Crio uma nova div com ID(errorForm)
                 */
                let returnError = $("<div>", {
                    id: "errorForm",
                    class: "bg-danger p-3 rounded text-center"
                })

                /**
                 * Atribuo o texto ao html a nova div errorForm
                 */
                returnError.html('<h3 class="text-white">Éhhh...</h3><p class="text-white">Todos os campos são obrigatórios. <strong>Digite algo!</strong></p>');

                /**
                 * Insero a nova div errorForm ao DOM
                 */
                $("#content").append(returnError)

                /**
                 * Removo a div que contém a class .result
                 */
                if ($(".result")) {
                    $(".result").remove()
                }

                /**
                 * Atribuo um title ao btn enviar
                 */
                $("#btn_send").attr('title', "Digite algo no campo para habilitar esse botão")
            }


        })
    </script>
</body>

</html>