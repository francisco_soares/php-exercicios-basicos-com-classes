<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Quando ganha por hora e mês</title>

    <!-- Style -->
    <link rel="stylesheet" href="../assets/style/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/style/style.css?v=<?= time(); ?>">

</head>

<body>

    <div class="container h-100">
        <div class="row h-100">
            <div class="formulario col-md-6 offset-md-3 align-self-center" id="conteudo">
                <h3 class="font-weight-bold text-center">Quando ganha por hora e mês</h3>
                <p class="mb-5 text-center">Informe no formulário abaixo seu valor em por hora e quantidade de horas trabalhada no mês.</p>

                <form action="../Config/controlador.php" method="POST" id="enviarForm">
                    <input type="hidden" name="exercicio" value="exercicio-9">

                    <div class="mb-3">
                        <label for="valor_hora" class="font-weight-bold">Valor hora:</label>
                        <input type="text" name="valor_hora" class="form-control" id="valor_hora" placeholder="Ex: 80 | 80.00">
                    </div>

                    <div class="mb-3">
                        <label for="horas_mes" class="font-weight-bold">Quantidade de horas trabalhada no mês.</label>
                        <input type="text" name="horas_mes" class="form-control" id="horas_mes" placeholder="Ex: 42">
                    </div>

                    <div class="mb-3">
                        <input type="submit" value="Enviar" class="btn btn-lg btn-outline-danger font-weight-bold w-100">
                    </div>
                </form>

                <?php if (isset($_GET['result']) && $_GET['result'] == 'error') { ?>
                    <div class="bg-warning p-3 rounded text-center result">
                        <h3 class="text-dark">OPSS...</h3>
                        <p class="text-dark"><strong>TODOS</strong> os campos devem ser preenchido!</p>
                    </div>
                <?php } elseif ((isset($_GET['valor_hora']) && $_GET['valor_hora'] != '') || (isset($_GET['horas_mes']) && $_GET['horas_mes'] != '')) { ?>
                    <div class="bg-success p-3 rounded  result">
                        <h3 class="text-white text-center">Resultado</h3>
                        <p class="text-white">
                            <strong>Valor da hora: </strong> R$ <?= number_format($_GET['valor_hora'], 2, ',', '.'); ?><br>
                            <strong>Quantidade de horas trabalhadas: </strong> <?= $_GET['horas_mes']; ?><br>
                            <strong>Salário total do mês: </strong> R$ <?= $_GET['result']; ?>
                        </p>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>


    <!-- Scripts -->
    <script src="../assets/style/bootstrap/js/bootstrap.min.js"></script>
    <script src="../assets/js/jquery-3.6.0.min.js"></script>

    <script>
        $("#enviarForm").submit(function(e) {

            // Variaveis
            let valor_hora = parseFloat($('#valor_hora').val())
            let horas_mes = parseFloat($('#horas_mes').val())
            let out

            if (isNaN(valor_hora) || isNaN(horas_mes)) {

                // Paro o envio do formulário
                e.preventDefault()

                if ($('#errorForm')) {
                    $('#errorForm').remove()
                }

                returnError = $('<div>', {
                    id: 'errorForm',
                    class: 'bg-danger p-3 rounded text-center'
                })

                returnError.html('<h3 class="text-white">Éhhh...</h3><p class="text-white">Insira apenas <strong>Números</strong>!</p>')

                $('#conteudo').append(returnError)

                if ($('.result')) {
                    $('.result').remove()
                }

                out = setTimeout(alertOut, 6000)
            }
        })

        function alertOut() {
            $('#errorForm').fadeOut(200)
        }
    </script>

</body>

</html>