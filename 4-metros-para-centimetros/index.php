<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Metros para centimetros</title>

    <!-- Style -->
    <link rel="stylesheet" href="../assets/style/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/style/style.css?v=<?= time(); ?>">
</head>

<body>

    <div class="container h-100">
        <div class="row h-100">
            <div class="formulario col-md-6 offset-md-3 align-self-center text-center" id="conteudo">

                <h3 class="font-weight-bold text-uppercase">Metros para centimetros</h3>
                <p class="mb-4">Informe no formulário abaixo o valor para ser convertido.</p>

                <form action="../Config/controlador.php" method="POST" id="enviarForm">
                    <input type="hidden" name="exercicio" value="exercicio-4">

                    <div class="mb-3">
                        <label for="num" class="font-weight-bold">Informe abaixo um número</label>
                        <input type="text" name="num" id="num" class="form-control form-control-lg" placeholder="Ex: 2">
                    </div>
                    <div class="mb-3 mt-3">
                        <input type="submit" value="Enviar" class="btn btn-outline-primary btn-lg w-100 text-uppercase font-weight-bold">
                    </div>
                </form>

                <?php if (isset($_GET['result']) && $_GET['result'] == 'error') { ?>
                    <div class="bg-warning p-3 rounded text-center result">
                        <h3 class="text-dark">OPSS...</h3>
                        <p class="text-dark"><strong>TODOS</strong> os campos devem ser preenchido!</p>
                    </div>

                <?php } elseif (isset($_GET['result'])) { ?>
                    <div class="bg-success p-3 rounded text-center result">
                        <h3 class="text-white">Resultado</h3>
                        <p class="text-white">
                            <strong>Centimetros: </strong> <?= $_GET['result']; ?><br>
                            <strong>Métros: </strong> <?= $_GET['metros']; ?>
                        </p>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>


    <!-- Scripts -->
    <script src="../assets/style/bootstrap/js/bootstrap.min.js"></script>
    <script src="../assets/js/jquery-3.6.0.min.js"></script>

    <script>
        /**
         * Pego a ação de submit do formulário
         */
        $("#enviarForm").submit(function(e) {

            // Pego os valores dos campos do formulário
            let num = parseFloat($('#num').val())

            if (isNaN(num)) {

                // Paro a ação de submit do formulário.
                e.preventDefault()

                if ($('#errorForm')) {
                    $('#errorForm').remove()
                }

                // Crio uma DIV com ID e CLASSES
                let returnError = $("<div>", {
                    id: 'errorForm',
                    class: 'bg-danger p-3 rounded text-center'
                })

                returnError.html('<h3 class="text-white">Éhhh...</h3><p class="text-white">Insira apenas <strong>Números</strong>!</p>')

                $("#conteudo").append(returnError)

                if ($('.result')) {
                    $('.result').remove()
                }

                let out

                out = setTimeout(alertOut, 5000)
            }
        })

        function alertOut() {
            $('#errorForm').fadeOut(800)
        }
    </script>

</body>

</html>