<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Calculado</title>

    <!-- Style -->
    <link rel="stylesheet" href="../assets/style/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/style/style.css?v=<?= time(); ?>">
</head>

<body>

    <div class="container h-100">
        <div class="row h-100">
            <div class="formulario col-md-6 offset-md-3 align-self-center text-center" id="conteudo">
                <h3 class="font-weight-bold text-uppercase">Calculadora</h3>
                <p class="mb-5">Faça seus calculos</p>

                <form action="../Config/controlador.php" method="POST" id="enviarForm">
                    <input type="hidden" name="exercicio" value="exercicio-2">

                    <div class="row">
                        <div class="col-md-4">
                            <input type="text" name="n1" id="n1" class="form-control" placeholder="Ex: 15">
                        </div>
                        <div class="col-md-4">
                            <select name="operador" class="custom-select" id="operador">
                                <option value="soma">+</option>
                                <option value="sub">-</option>
                                <option value="mult">*</option>
                                <option value="div">/</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <input type="text" name="n2" id="n2" class="form-control" placeholder="Ex: 5">
                        </div>
                    </div>
                    <div class="mb-3 mt-3">
                        <input type="submit" value="Enviar calculo" class="btn btn-outline-danger w-100 btn-lg font-weight-bold text-uppercase">
                    </div>
                </form>

                <?php
                if (isset($_GET['result']) && $_GET['result'] == 'error') { ?>
                    <div class="bg-warning p-3 rounded text-center result">
                        <h3 class="text-dark">OPSS...</h3>
                        <p class="text-dark"><strong>TODOS</strong> os campos devem ser preenchido!</p>
                    </div>
                <?php } elseif (isset($_GET['operador']) && $_GET['operador'] === 'soma') { ?>
                    <div class="bg-success p-3 rounded text-center result">
                        <h3 class="text-white">Resultado</h3>
                        <p class="text-white">
                            <strong>Operador: </strong> <?= $_GET['operador'] ?><br>
                            <strong>Total: </strong> <?= $_GET['result'] ?>
                        </p>
                    </div>
                <?php } elseif (isset($_GET['operador']) && $_GET['operador'] == 'sub') { ?>
                    <div class="bg-success p-3 rounded text-center result">
                        <h3 class="text-white">Resultado</h3>
                        <p class="text-white">
                            <strong>Operador: </strong> <?= $_GET['operador'] ?><br>
                            <strong>Total: </strong> <?= $_GET['result'] ?>
                        </p>
                    </div>
                <?php } elseif (isset($_GET['operador']) && $_GET['operador'] == "mult") { ?>
                    <div class="bg-success p-3 rounded text-center result">
                        <h3 class="text-white">Resultado</h3>
                        <p class="text-white">
                            <strong>Operador: </strong> <?= $_GET['operador'] ?><br>
                            <strong>Total: </strong> <?= $_GET['result'] ?>
                        </p>
                    </div>
                <?php } elseif (isset($_GET['operador']) && $_GET['operador'] == 'div') { ?>
                    <div class="bg-success p-3 rounded text-center result">
                        <h3 class="text-white">Resultado</h3>
                        <p class="text-white">
                            <strong>Operador: </strong> <?= $_GET['operador'] ?><br>
                            <strong>Total: </strong> <?= $_GET['result'] ?>
                        </p>
                    </div>
                <?php } ?>


            </div>
        </div>
    </div>


    <!-- Scripts -->
    <script src="../assets/style/bootstrap/js/bootstrap.min.js"></script>
    <script src="../assets/js/jquery-3.6.0.min.js"></script>

    <script>
        /**
         * Pego a ação de submit do formulário
         */
        $("#enviarForm").submit(function(e) {

            // Pego os valores dos campos do formulário
            let n1 = parseFloat($('#n1').val())
            let n2 = parseFloat($('#n2').val())

            /**
             * Verifico se os valores passado é NaN(Not-A-Number), atenda uma dessa condição, entra dentro do laço.
             */
            if (isNaN(n1) || isNaN(n2)) {

                // Paro a ação de submit do formulário.
                e.preventDefault()

                // Crio uma DIV com ID e CLASSES
                let returnError = $("<div>", {
                    id: "errorForm",
                    class: 'bg-danger p-3 rounded text-center'
                })

                // Verifico se a div ID #errorForm exista. Caso exista, entro na condição e removo.
                if ($('#errorForm')) {
                    $('#errorForm').remove()
                }

                // Insiro alguns textos na DIV #errorForm que defini acima.
                returnError.html('<h3 class="text-white">Éhhh...</h3><p class="text-white">Insira apenas <strong>Números</strong>!</p>')

                // Insiro a DIV #errorForm após todos os filhos da DIV ID pai #conteudo
                $("#conteudo").append(returnError)

                if ('.result') {
                    $('.result').fadeOut(1)
                }

                let out
                out = setTimeout(alertOut, 4000)
            }
        })

        /**
         * Função que apenas apaga a DIV         
         */
        function alertOut() {
            $('#errorForm').fadeOut(800)
        }
    </script>
</body>

</html>