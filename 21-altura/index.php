<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Altura que você quer chegar</title>

    <!-- Style -->
    <link rel="stylesheet" href="../assets/style/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/style/style.css?v=<?= time(); ?>">
</head>

<body>

    <div class="container h-100">
        <div class="row h-100">
            <div class="formulario col-md-6 offset-md-3 align-self-center" id="content">

                <h3 class="text-center font-weight-bold">Altura que você quer chegar</h3>
                <p class="text-center">Digite no formulário abaixo sua altura e a altura que quer chegar</p>


                <form action="../Config/controlador.php" method="POST" id="sendForm">

                    <input type="hidden" name="exercicio" value="exercicio-21">

                    <div class="mb-4">
                        <label for="height">Informe sua altura</label>
                        <input type="text" name="height" id="height" class="form-control" placeholder="Ex: 1.70">
                    </div>

                    <div class="mb-4">
                        <label for="height_you_want">Informe a altura que quer chegar</label>
                        <input type="text" name="height_you_want" id="height_you_want" class="form-control" placeholder="Ex: 1.80">
                    </div>

                    <div class="mb-4">
                        <input type="submit" value="Enviar" class="btn btn-lg btn-outline-danger font-weight-bold w-100" id="btn_send">
                    </div>
                </form>

                <?php if (isset($_GET['result']) && $_GET['result'] == "error") { ?>
                    <div class="bg-warning p-3 rounded text-center result">
                        <h3 class="text-dark">OPSS...</h3>
                        <p class="text-dark"><strong>TODOS</strong> os campos devem ser preenchido!</p>
                    </div>
                <?php } ?>

                <?php if (isset($_GET['result']) && $_GET['result'] && isset($_GET['year'])) { ?>
                    <div class="bg-success p-3 rounded text-center result">
                        <h3 class="text-white font-weight-bold">Resultado</h3>
                        <p class="text-white">
                            Quantidade de tempo para alcançar a altura de <strong><?= $_GET['height_you_want']; ?></strong>: <br>
                            <strong><?= $_GET['year']; ?></strong> ano(s)
                        </p>
                        <p class="text-white">Com essa estimativa de <strong> <?= $_GET['year']; ?> anos(s)</strong>, você chegará a <strong><?= $_GET['current_height']; ?></strong> de altura.</p>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <!-- Scripts -->
    <script src="../assets/style/bootstrap/js/bootstrap.min.js"></script>
    <script src="../assets/js/jquery-3.6.0.min.js"></script>

    <script>
        $(document).ready(function() {

            /** ==== [INÍCIO]: Váriaveis declaradas */
            let height = $('#height').val();
            let height_you_want = $('#height_you_want').val();
            /** [FIM]: ==== */

            /** ==== [INÍCIO]: Bloco que verifica assim que acessa a página se o campo input do form está vázio. */
            if (height == '' || height_you_want == '') {
                $('#btn_send').prop('disabled', true);
            }
            /** [FIM]: ==== */

            /** ==== [INICIO]: Bloco que verifica o que está sendo digitado, mais detalhes dentro dessa função. */
            $('#sendForm').keyup(function() {
                /** ==== [INÍCIO]: Váriaveis declaradas */
                let keyup_height = $("#height").val();
                let keyup_height_you_want = $("#height_you_want").val();
                /** [FIM]: ==== */

                if($('#errorForm')){
                    $('#errorForm').remove();
                }

                // Verifico se o valor digitado é uma string, se sim o isNaN() retorna true e entra no if().
                if(isNaN(keyup_height) || isNaN(keyup_height_you_want) || keyup_height == "" || keyup_height_you_want == ""){
                    // Criado uma div para apresentar o erro.
                    let returnError = $("<div>", {
                        id: "errorForm",
                        class: "bg-danger p-3 rounded text-center"
                    });

                    /** Desabilito o botão de enviar */
                    $("#btn_send").prop("disabled", true);

                    // Verifico se a div result está sendo mostrada. Caso sim, entra no if e exclue a mesma.
                    if($('.result')){
                        $('.result').remove();
                    }

                    // Insiro o texto de erro na div criada acima (returnError).
                    returnError.html('<h3 class="text-white">Éhhh...</h3><p class="text-white">Insira apenas <strong>Números separados por ponto(.).</strong></p>');

                    $("#content").append(returnError);
                } else {
                    $("#btn_send").prop("disabled", false);
                }
                
            });
            /** [FIM]: ==== */
        });
    </script>
</body>

</html>