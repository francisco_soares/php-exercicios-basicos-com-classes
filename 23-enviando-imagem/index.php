<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Envio de imagem</title>

    <!-- Style -->
    <link rel="stylesheet" href="../assets/style/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/style/style.css?v=<?= time(); ?>">

</head>

<body>

    <div class="container h-100">
        <div class="row h-100">
            <div class="formulario col-md-6 offset-md-3 align-self-center" id="content">

                <h3 class="text-center font-weight-bold">Envio de imagem</h3>
                <p class="text-center">Informe o nome da imagem e envie uma imagem nos formatos, JPG, PNG ou GIF</p>

                <form action="../Config/controlador.php" method="POST" id="sendForm" enctype="multipart/form-data">
                    <input type="hidden" name="exercicio" value="exercicio-23">

                    <div class="mb-2">
                        <label for="file_name">Nome da imagem: <span>*</span></label>
                        <input type="text" name="file_name" id="file_name" class="form-control" placeholder="Informe um nome para a imagem.">
                    </div>

                    <div class="mb-4">
                        <label for="file_img">Imagem: <span>*</span></label>
                        <div class="custom-file">
                            <input type="file" name="file_img" class="custom-file-input" id="file_img" aria-describedby="file_img">
                            <label for="file_img" class="custom-file-label" id="label_input_file">Enviar imagem</label>
                        </div>
                    </div>

                    <div class="mb-4">
                        <input type="submit" value="Enviar" class="btn btn-lg btn-outline-danger font-weight-bold w-100" id="btn_send">
                    </div>
                </form>

                <?php if (isset($_GET['result']) && $_GET['result'] == "errorEmpty") { ?>
                    <div class="bg-warning p-3 rounded text-center result">
                        <h3 class="text-dark font-weight-bold">OPSS...</h3>
                        <p class="text-dark"><strong>TODOS</strong> os campos devem ser preenchido!</p>
                    </div>
                <?php } ?>

                <?php if (isset($_GET['result']) && $_GET['result'] == "errorFormat") { ?>
                    <div class="bg-warning p-3 rounded text-center result">
                        <h3 class="text-dark font-weight-bold">OPSS...</h3>
                        <p class="text-dark"><strong>FORMATO</strong> não aceito, por favor envie uma imagem em JPG, PNG ou GIF!</p>
                    </div>
                <?php } ?>

                <?php if (isset($_GET['result']) && $_GET['result'] == "erro-upload") { ?>
                    <div class="bg-danger p-3 rounded text-center result">
                        <h3 class="text-white font-weight-bold">OPSS...</h3>
                        <p class="text-white">Não foi possível fazer o upload da imagem, tente novamente mais tarde!</p>
                    </div>
                <?php } ?>

                <?php if (isset($_GET['result']) && $_GET['result'] == "success") { ?>
                    <div class="bg-success p-3 rounded text-center result">
                        <h3 class="text-white font-weight-bold">SUCESSOOO...</h3>
                        <p class="text-white">Upload da sua imagem foi enviado.</p>
                    </div>
                <?php } ?>

            </div>
        </div>
    </div>

    <!-- Scripts -->
    <script src="../assets/style/bootstrap/js/bootstrap.min.js"></script>
    <script src="../assets/js/jquery-3.6.0.min.js"></script>

    <script>
        $(document).ready(function() {
            /** ==== [INÍCIO]: Váriaveis declaradas */
            let file_name = $("#file_name").val();
            let file_img = $("#file_img").val();

            if (file_img.length == 0) {
                $("#btn_send").prop('disabled', true);
            }

            $("#file_img").change(function(e) {

                /** ==== [INÍCIO]: Váriaveis declaradas */
                let fileName = e.target.files[0].name; // pego o nome da imagem com a sua extensão, ex: imagem.png
                let fileType = e.target.files[0].type; // Pego o tipo/type do arquivo/file, ex: image/png, image/jpg e etc.
                let arrFile = [
                    "image/jpeg",
                    "image/png",
                    "image/gif"
                ];
                /** [FIM]: ==== */

                // Se a div ID #errorForm estiver presente no front, será removida
                if($("#errorForm")){
                    $("#errorForm").remove();
                }

                // Se a div CLASS .result estiver presente no front, será removida
                if($(".result")){
                    $(".result").remove();
                }

                /* Verifico se o tipo do arquivo não existe, se não existir, entra no if() */
                if ($.inArray(fileType, arrFile) == -1) {
                    
                    // Crio a DIV ID errorForm
                    let erroFile = $("<div>", {
                        id: "errorForm",
                        class: "bg-danger p-3 rounded text-center"
                    });

                    // Desabilito o botão de enviar
                    $("#btn_send").prop("disabled", true);
                    $("#label_input_file").html("Enviar imagem");

                    // Inserido tags html com a mensagem.
                    erroFile.html('<h3 class="text-white">Éhhh...</h3><p class="text-white">OPSSS... Formato do arquivo incorreto, favor enviar arquivo em <strong>JPG, PNG ou GIF</strong></p>');

                    // Insira o conteúdo, especificado pelo parâmetro, no final de cada elemento no conjunto de elementos correspondentes. 
                    // Font: https://api.jquery.com/append/
                    $("#content").append(erroFile);

                    // Retiro o valor do campo INPUT FILE.
                    $("#file_img").val("");

                } else {
                    // Habilito o btn de enviar
                    $("#btn_send").prop("disabled", false);
                    // Insiro o nome da imagem junto com sua extensão, ex: saitama.png
                    $("#label_input_file").html(fileName);
                }
            });
            /** [FIM]: ==== */
        });
    </script>
</body>

</html>