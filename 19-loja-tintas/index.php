<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Loja de Tintas</title>

    <!-- Style -->
    <link rel="stylesheet" href="../assets/style/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/style/style.css?v=<?= time(); ?>">
</head>

<body>
    <div class="container h-100">
        <div class="row h-100">
            <div class="formulario col-6-md offset-md-3 align-self-center" id="content">

                <h3 class="text-center font-weight-bold">Loja de Tintas</h3>
                <p>Para comprar a tinta, informe o metro quadrado.</p>

                <form action="../Config/controlador.php" method="POST" id="sendForm">
                    <input type="hidden" name="exercicio" value="exercicio-19">

                    <div class="mb-4">
                        <label for="metro_quadrado">Informe o metro quadrado</label>
                        <input type="text" name="metro_quadrado" id="metro_quadrado" class="form-control" placeholder="Ex: 5">
                    </div>

                    <div class="mb-4">
                        <input type="submit" value="Enviar" class="btn btn-lg btn-outline-danger font-weight-bold w-100" id="btn_send">
                    </div>
                </form>

                <?php if (isset($_GET['result']) && $_GET['result'] == 'error') { ?>
                    <div class="bg-warning p-3 rounded text-center result">
                        <h3 class="text-dark">OPSS...</h3>
                        <p class="text-dark"><strong>TODOS</strong> os campos devem ser preenchido!</p>
                    </div>
                <?php } ?>

                <?php if (isset($_GET['qtd_litro']) && isset($_GET['qtd_latas']) && isset($_GET['valor_total'])) { ?>
                    <div class="bg-success p-3 rounded text-center result">
                        <h3 class="text-white font-weight-bold">Resultado</h3>
                        <p class="text-white">
                            <strong>Qtd Litro:</strong> <?= $_GET['qtd_litro']; ?><br>
                            <strong>Qtd Lata(s):</strong> <?= $_GET['qtd_latas']; ?><br>
                            <strong>Valor Total:</strong> R$ <?= $_GET['valor_total']; ?><br>
                        </p>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>

    <!-- Scripts -->
    <script src="../assets/style/bootstrap/js/bootstrap.min.js"></script>
    <script src="../assets/js/jquery-3.6.0.min.js"></script>

    <script>
        $(document).ready(function() {
            /** ==== [INICIO]: Bloco que verifica assim que acessa a página se o campo input do form está vázio. */
            let metro_quadrado = $('#metro_quadrado').val()

            if (metro_quadrado == "") {
                $('#btn_send').prop('disabled', true);
            }
            /** [FIM]: ==== */

            /** ==== [INICIO]: Bloco que verifica o que está sendo digitado, mais detalhes dentro dessa função. */
            $("#sendForm").keyup(function() {

                // Variaveis
                let m_quadrado = $('#metro_quadrado').val();
                
                // Código abaixo serve para Debug.
                console.log(!isNaN(+m_quadrado));

                /* EXPLICAÇÃO IMPORTANTE: 
                [m_quadrado] = Verifica se tem algo digitado na variavel.
                [!isNaN(+ m_quadrado)] = Verifica se o valor digita é numero, caso seja será retornado TRUE por conta do (!) que está negando o FALSE.
                Como é retornado TRUE, entra na condição e habilita o btn.
                */
                if (m_quadrado || !isNaN(+m_quadrado)) {
                    $("#btn_send").prop("disabled", false);
                }

                /* Verifico se o ID = errorForm existe, caso seja verdadeiro, entra na condição e remove o mesmo. */
                if($("#errorForm")){
                    $("#errorForm").remove();
                }

                /* EXPLICAÇÃO IMPORTANTE: 
                [m_quadrado] = Verifica se tem algo digitado na variavel.
                [isNaN(+ m_quadrado)] = Verifica se o valor digita é string, caso seja será retornado TRUE.
                Como é retornado TRUE, entra na condição e desabilita o btn.
                */
                if (m_quadrado == "" || isNaN(+m_quadrado) == true) {
                    $('#btn_send').prop("disabled", true);
                    $("#metro_quadrado").val('');

                    // Crio uma div com ID = errorForm e uma class = bg-danger p-3 rounded text-center (BOOTSTRAP)
                    let returnError = $("<div>", {
                        id: "errorForm",
                        class: "bg-danger p-3 rounded text-center"
                    });

                    // Verifico se a class result existe no código. Caso exista no momento de execução, eu a removo.
                    if ($('.result')) {
                        $('.result').remove();
                    }

                    // Apresenta o erro na tela.
                    returnError.html('<h3 class="text-white">Éhhh...</h3><p class="text-white">Insira apenas <strong>Números!</strong></p>');
                    $("#content").append(returnError);
                }

            });
        });
    </script>
</body>

</html>