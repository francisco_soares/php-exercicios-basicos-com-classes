<?php

namespace Config\Model;

class metrosParaCentimetros
{
    private $num;
    private $result;

    public function setNum($num)
    {
        $this->num = $num;
    }

    public function getNum()
    {
        $this->result = $this->num * 100;

        return $this->result;
    }
}
