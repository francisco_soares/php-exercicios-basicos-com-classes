<?php

namespace Config\Model;

class areaQuadrado
{
    // Atributos privados
    private $num;
    private $result;

    public function setAreaQuadrado($num)
    {
        $this->num = $num;
    }

    public function getAreaQuadrado()
    {
        $this->result = $this->num * $this->num;

        return $this->result;
    }
}
