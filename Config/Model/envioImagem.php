<?php

namespace Config\Model;

class envioImagem
{
    /* Atributo da Classse */
    private $name_file = null;
    private $file_img = [];

    public function __construct($name_file = null, array $file_img)
    {
        $this->name_file = $name_file;
        $this->file_img  = $file_img;
    }

    /**
     * Método que retorna o resultado do envio das imagem
     *
     * @return void
     */
    public function getResult()
    {

        if (!empty($this->name_file)) {
            $img_name = $this->handleEspecialCharacters($this->name_file, $this->file_img['name']);

            return $this->uploadImage($img_name);
        } else {

            $imgFile = $this->handleFilename($this->file_img['name']);

            return $this->uploadImage($imgFile);
        }
    }

    /**
     * Função que recebe dois parametro
     * $name_img = Nome informado no campo input name
     * $file_extension = Nome da imagem com sua extensão imagem.PNG ($this->file_img['name'])
     * 
     * Método que faz todas as tratativas dos caracteres especiais com a passagem dos dois parametros
     *
     * @param [string] $name_img
     * @param [array] $file_extension
     * @return string
     */
    private function handleEspecialCharacters($name_img, $file_extension)
    {
        // Aqui faço uma tratativa para retirar caracteres especiais
        $com_acentos = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜüÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿRr"!@#$%&*()+={[}]/?;:,\\\'<>°ºª ';
        $sub_sem_acento = 'aaaaaaaceeeeiiiidnoooooouuuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr_______________________________';
        return strtr(utf8_decode($name_img), utf8_decode($com_acentos), $sub_sem_acento) . "_" . time() . mb_strstr($file_extension, '.');
    }

    /**
     * Função que recebe um parametro
     * $name_file = Nome da imagem com sua extensão imagem.PNG ($this->file_img['name'])
     * 
     * Método que faz todas as tratativas do nome da imagem
     *
     * @param [type] $name_file
     * @return void
     */
    private function handleFilename($name_file)
    {
        /**
         * EXPLICAÇÃO
         * A função mb_strstr(<string>, <referencia>) retorna uma string a partir da referêcia que você determinar.
         * Ex: 
         * $cv = "meu-curriculo.pdf"
         * echo mb_strstr($cv, '.');
         * 
         * RETORNO: .pdf
         */
        $img     = str_replace(mb_strstr($name_file, '.'), "", $name_file);
        $imgFile = $img . "_" . time() . mb_strstr($name_file, '.');
        $imgFile = str_replace('-', '_', $imgFile);
        $imgFile = str_replace(' ', '_', $imgFile);
        $imgFile = str_replace('  ', '_', $imgFile);

        return $imgFile;
    }

    /**
     * Método que recebe um parametro e faz o upload da imagem
     *
     * @param [string] $name_img
     * @return void
     */
    private function uploadImage($name_img)
    {
        // Caminho do diretório
        $dir = "../23-enviando-imagem/img";

        /* A função [file_exists] é uma função integrada para verificar onde um diretório ou um arquivo existe ou não. Ela aceita um parâmetro de um caminho que retorna true se ele já existe ou false se não existir. */

        /* [is_dir] - Esta função também é semelhante a file_exists, e a única diferença é que ela só retornará true se a string passada for um diretório e retornará false se for um arquivo. */

        if (!file_exists($dir) || !is_dir($dir)) {
            mkdir($dir, 0755);
        }

        // Uso a função move_uploaded_file(), caso upload aconteça, retorna true e move o arquivo... caso contrário, da erro.
        if (move_uploaded_file($this->file_img['tmp_name'], "{$dir}/{$name_img}")) {
            return true;
        } else {
            return false;
        }
    }
}
