<?php

namespace Config\Model;

class bibliotecaUniversidade
{
    // Atributos da class
    private $name;
    private $book;
    private $choice;
    private $result;

    public function __construct($name, $book, $choice)
    {
        $this->name = $name;
        $this->book = $book;
        $this->choice = $choice;
    }

    public function result()
    {
        if ($this->choice == "student") {
            $this->result = [
                'days' => 3,
                'name' => $this->name,
                'book' => $this->book
            ];
        } else {
            $this->result = [
                'days' => 10,
                'name' => $this->name,
                'book' => $this->book
            ];
        }

        return $this->result;
    }
}
