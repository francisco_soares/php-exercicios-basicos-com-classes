<?php

namespace Config\Model;

class farenheitParaCelsius
{
    // Atributo da classe
    private $num;
    private $result;

    public function setCalc($num)
    {
        $this->num = $num;
    }

    public function getCalc()
    {
        $this->result = ($this->num - 32) * 5 / 9;
        return $this->result;
    }
}
