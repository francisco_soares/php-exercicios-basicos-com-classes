<?php

namespace Config\Model;

class lerNumeros
{

    /* Variaveis */
    private $number = [];
    private $arr_number = [];

    /* Método construtor */
    public function __construct(array $number)
    {
        $this->number = $number;
    }

    /**
     * Método que faz uma validação em busca de um valor = 0 dentro do array.
     * Caso tenha um valor = 0, será retornado os valores anteriores dentro do array junto com o 0.
     * Os valores após o 0 não será retornado.
     *
     * @return Array
     */
    public function getResult(){

        

        foreach($this->number as $num){

            /* Verifico se no array tem valor string, caso tenha, coloco o erro_string e os valores string dentro de um array e envio para tratar no controlador.php. */
            if(floatval($num) == 0){
                array_push($this->arr_number, $num);
                return $this->arr_number;
                die;
            }

            array_push($this->arr_number, $num);
        }

        return $this->arr_number;
    }

}
