<?php

namespace Config\Model;

class consultaAPI
{
    private $url_api;
    private $curl_api;
    private $result;

    public function __construct($url_api)
    {
        $this->url_api  = $url_api;
        $this->handlesRequisition();
    }

    public function getResult()
    {
        return $this->result;
    }

    private function handlesRequisition()
    {
        // Inicio o cURL e passo a url da API, isso seria a variavel $url_api
        $this->curl_api = curl_init($this->url_api);

        // Convertendo para Array
        curl_setopt($this->curl_api, CURLOPT_RETURNTRANSFER, true);

        // Verificando o SSL, como está definido como false, ele não verifica se tem SSL
        curl_setopt($this->curl_api, CURLOPT_SSL_VERIFYPEER, false);

        // Tipo de requisição
        curl_setopt($this->curl_api, CURLOPT_CUSTOMREQUEST, "GET");

        // Executo o cURL com o método curl_exec(), passando por parametro o cURL iniciada[$curl_api]
        // Na variavel $result, o mesmo vai trazer um obj.
        $this->result = json_decode(curl_exec($this->curl_api));
    }
}
