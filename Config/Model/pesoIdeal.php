<?php

namespace Config\Model;

class pesoIdeal
{
    private $sexo;
    private $altura;
    private $result;

    public function __construct($sexo, $altura)
    {
        $this->sexo = $sexo;
        $this->altura = $altura;
    }

    public function getCalc()
    {
        if ($this->sexo == 'F') {
            $this->result = (62.1 * $this->altura) - 44.7;
        } else {
            $this->result = (72.7 * $this->altura) - 58;
        }

        return $this->result;
    }
}
