<?php

namespace Config\Model;

class celsiusParaFarenheit
{
    // Atributos da classe
    private $num;
    private $result;

    public function setCalc($num)
    {
        $this->num = $num;
    }

    public function getCalc()
    {
        $this->result = ($this->num * 9 / 5) + 32;
        return $this->result;
    }
}
