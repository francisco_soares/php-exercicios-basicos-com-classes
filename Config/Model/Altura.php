<?php

namespace Config\Model;

// Chico tem 1,50m e cresce 2 centímetros por ano, enquanto Juca tem 1,10m e cresce 3 centímetros por ano. Construir um algoritmos que calcule e imprima quantos anos serão necessários para que Juca seja maior que Chico

class Altura
{
    // Atribute
    private $height;
    private $height_you_want;

    public function __construct($height, $height_you_want)
    {
        $this->height = $height;
        $this->height_you_want = $height_you_want;
    }

    public function getResult()
    {
        $i = 0;
        while($this->height <= $this->height_you_want) {
            $this->height = $this->height + 0.03;
            $i++;
        }

        $arr_year = [
            "year"            => $i,
            "current_height"  => number_format($this->height, 2),
            "intended_height" => number_format($this->height_you_want, 2)
        ];

        return $arr_year;
    }
}
