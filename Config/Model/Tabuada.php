<?php

namespace Config\Model;

class Tabuada
{
    // Atributos da classe.
    private $n1;
    private $n2;
    private $result;

    /**
     * Método que recebe os valores passado pelo formulário da Tabuada
     *
     * @param float $n1
     * @param float $n2     
     */
    public function setTabuada($n1, $n2)
    {
        $this->n1 = $n1;
        $this->n2 = $n2;
    }

    /**
     * Método com a lógica da tabuada.
     *
     * @return string
     */
    public function getTabuada()
    {

        // Lógica da tabuada
        for ($i = 1; $i <= $this->n2; $i++) {
            $this->result .= $this->n1 . " x " . $i . " = " . $this->n1 * $i . "<br>";
        }

        return $this->result;
    }
}
