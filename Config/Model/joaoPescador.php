<?php

namespace Config\Model;

class joaoPescador
{
    // Atributos privador
    private $weight;
    private $weightExceeded;
    private $result;
    private $fineAmount;

    public function __construct($weight)
    {
        $this->weight = $weight;
    }

    public function getCalc()
    {
        if ($this->weight > 50) {
            $this->weightExceeded = $this->weight - 50;
            $this->fineAmount = $this->weightExceeded * 4;
        }

        $returnResult = array(
            "weight" => $this->weight, 
            "weightExceeded" => $this->weightExceeded,
            "fineAmount" => $this->fineAmount
        );

        return $returnResult;
    }
}
