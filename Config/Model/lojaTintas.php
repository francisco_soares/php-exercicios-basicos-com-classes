<?php

namespace Config\Model;

class lojaTintas
{

    // PROBLEMA PARA SER RESOLVIDO:
    /* Faça um script para uma loja de tintas. O script deverá pedir o tamanho em metros quadrados da área a ser pintada. Considere que a cobertura da tinta é de 1 litro para cada 3 metros quadrados e que a tinta é vendida em latas de 18 litros, que custam R$ 80,00. Informe ao usuário a quantidades de latas de tinta a serem compradas e o preço total. */

    // Atributos da classe
    private $metro_quadrado;
    private $litro = 3;
    private $lata = 18;
    private $valor_lata = 80;
    private $qtd_litro = 0;
    private $qtd_latas = 0;
    private $valor_total = 0;

    public function __construct($metro_quadrado)
    {

        $this->metro_quadrado = $metro_quadrado;

        $this->calc();
    }

    public function result()
    {
        return $this->calc();
    }

    private function calc()
    {
        // Pego a quantidade litro
        $this->qtd_litro = $this->metro_quadrado / $this->litro;
        // Calculo a quantidade de latas
        $this->qtd_latas = $this->qtd_litro / $this->lata;
        // Faço o calculo do valor total
        $this->valor_total = number_format($this->valor_lata * ceil($this->qtd_latas), 2, ',', '.');

        return array("qtd_litro" => ceil($this->qtd_litro), "qtd_latas" => ceil($this->qtd_latas), "valor_total" => $this->valor_total);
    }
}
