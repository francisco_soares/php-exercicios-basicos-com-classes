<?php

namespace Config\Model;

class calculoHorasTrabalhadas
{
    // Atributos privados da classe
    private $valor_hora;
    private $horas_mes;
    private $result;

    public function __construct($valor_hora, $horas_mes)
    {
        $this->valor_hora = $valor_hora;
        $this->horas_mes = $horas_mes;
    }

    public function getCalc()
    {
        $this->result = $this->valor_hora * $this->horas_mes;
        return $this->result;
    }
}
