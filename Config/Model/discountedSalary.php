<?php

namespace Config\Model;

class discountedSalary
{

    // private Attributes
    private $value_hour;
    private $month_worked;
    private $gross_salary;
    private $ir;
    private $inss;
    private $syndicate;
    private $total_discount;
    private $net_salary;

    public function __construct($value_hour, $month_worked)
    {
        $this->value_hour = $value_hour;
        $this->month_worked = $month_worked;
    }

    public function getCalc()
    {
        // Calc Gross Salary
        $this->gross_salary = $this->month_worked * $this->value_hour;
        // Calc IR
        $this->ir = 11 * $this->gross_salary / 100;
        // Calc INSS
        $this->inss = 8 * $this->gross_salary / 100;
        // Calc SYNDICATE
        $this->syndicate = 5 * $this->gross_salary / 100;
        // Calc total discount
        $this->total_discount = $this->ir + $this->inss + $this->syndicate;
        // Calc net salary
        $this->net_salary = $this->gross_salary - $this->total_discount;

        $returnResult = array(
            "gross_salary" => $this->gross_salary,
            "net_salary" => $this->net_salary,
            "ir" => $this->ir,
            "inss" => $this->inss,
            "syndicate" => $this->syndicate
        );

        return $returnResult;
    }
}
