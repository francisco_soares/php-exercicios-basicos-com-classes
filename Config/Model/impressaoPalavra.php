<?php

namespace Config\Model;

class impressaoPalavra
{
    // Atributos privados
    private $word;
    private $result;

    public function __construct($word)
    {
        $this->word = $word;
    }

    public function getWord()
    {
        return $this->word;
    }
}
