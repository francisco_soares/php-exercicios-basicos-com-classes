<?php

namespace Config\Model;

class GasolinaCalculo
{
    private $preco_litro;
    private $consumo;
    private $distancia;

    public function __construct($preco_litro, $distancia, $consumo = NULL)
    {
        $this->preco_litro  = $preco_litro;
        $this->consumo      = $consumo;
        $this->distancia    = $distancia;
    }

    public function getCalcLitro()
    {
        return number_format($this->preco_litro * $this->distancia, 2, ',', '.');
    }

    public function getCalcConsumo() 
    {
        return number_format($this->getConsumo() * $this->preco_litro, 2, ',', '.');
    }

    public function getConsumo()
    {
        return $this->distancia / $this->consumo;
    }

    public function getKm()
    {
        return $this->distancia;
    }
}
