<?php

namespace Config\Model;

class somaDoisNumeros
{
    // Private attributes
    private $number_1;
    private $number_2;
    private $total;
    private $operator;
    private $result;

    public function __construct($number_1, $number_2)
    {
        $this->number_1 = $number_1;
        $this->number_2 = $number_2;
    }

    public function getCalc()
    {
        $this->total = $this->number_1 + $this->number_2;

        if ($this->total > 20) {
            $this->total = $this->total + 8;
            $this->operator  = "somado";

            $this->result = [
                'total' => $this->total,
                'operator' => $this->operator,
                'assigned' => 8
            ];

            return $this->result;
        } else {
            $this->total = $this->total - 5;
            $this->operator = "subtraido";

            $this->result = [
                'total' => $this->total,
                'operator' => $this->operator,
                'assigned' => 5
            ];
            return $this->result;
        }
    }
}
