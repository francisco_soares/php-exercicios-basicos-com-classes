<?php

namespace Config\Model;

class arquivoXml
{
    // Atributos privados
    private $book_title;
    private $genero_book;
    private $author_book;
    private $price_book;
    private $content_book = array();

    public function __construct($book_title, $genero_book, $author_book, $price_book)
    {
        $this->book_title = trim($book_title);
        $this->genero_book = trim($genero_book);
        $this->author_book = trim($author_book);
        $this->price_book = trim($price_book);
    }

    public function getBook()
    {
        if ($this->book_title == "" || $this->genero_book == "" || $this->author_book == "" || $this->price_book == "") {
            return false;
        } else {
            return $this->content_book = ["book_title" => $this->book_title, "genero_book" => $this->genero_book, "author_book" => $this->author_book, "price_book" => $this->price_book];
        }
    }
}
