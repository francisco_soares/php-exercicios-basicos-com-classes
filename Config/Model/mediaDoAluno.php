<?php

namespace Config\Model;

class mediaDoAluno
{

    // Atributos da classe.
    private $n1;
    private $n2;
    private $n3;
    private $result;

    /**
     * Método contrutor da Classe que recebe 3 parametros.
     *
     * @param float $n1
     * @param float $n2
     * @param float $n3
     */
    public function __construct($n1 = null, $n2 = null, $n3 = null)
    {
        $this->n1 = $n1;
        $this->n2 = $n2;
        $this->n3 = $n3;
    }

    /**
     * Método da class responsável por fazer o calculo e retornar a média do aluno.
     *
     * @return float
     */
    public function getCalc()
    {
        $this->result = ($this->n1 + $this->n2 + $this->n3) / 3;

        return $this->result;
    }
}
