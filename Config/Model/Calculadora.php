<?php

namespace Config\Model;

class Calculadora
{

    // Atributo da classe
    private $n1;
    private $n2;
    private $operador;
    private $result;

    public function __construct($n1, $n2, $operador)
    {
        $this->n1       = $n1;
        $this->n2       = $n2;
        $this->operador = $operador;
    }

    public function getCalc()
    {
        if ($this->operador === "soma") {
            return $this->result = $this->n1 + $this->n2;
        } elseif ($this->operador === "sub") {
            return $this->result = $this->n1 - $this->n2;
        } elseif ($this->operador === "mult") {
            return $this->result = $this->n1 * $this->n2;
        } else {
            return $this->result = $this->n1 / $this->n2;
        }
    }
}
