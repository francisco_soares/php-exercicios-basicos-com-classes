<?php

namespace Config\Model;

class divisivelPorDezCincoDois
{

    // Atributos da classe
    private $number_1;
    private $result_two;
    private $result_five;
    private $result_ten;
    private $total_result;

    public function __construct($number_1)
    {
        $this->number_1 = $number_1;
    }

    public function getCalc()
    {
        $this->result_two = $this->number_1 % 2;
        $this->result_five = $this->number_1 % 5;
        $this->result_ten = $this->number_1 % 10;


        if ($this->result_two == 0) {
            $this->result_two = true;
        }else{
            $this->result_two = false;
        }

        if ($this->result_five == 0) {
            $this->result_five = true;
        }else{
            $this->result_five = false;
        }

        if ($this->result_ten == 0) {
            $this->result_ten = true;
        }else{
            $this->result_ten = false;
        }

        $this->total_result = [
            "result_two" => $this->result_two,
            "result_five" => $this->result_five,
            "result_ten" => $this->result_ten,
            "number_send" => $this->number_1
        ];

        return $this->total_result;
    }
}
