<?php

namespace Config\Model;

class Curriculo
{
    // Atributos
    private $name;
    private $email;
    private $whatsapp;
    private $cv = [];

    public function __construct($name, $email, $whatsapp, array $cv)
    {
        $this->name     = $name;
        $this->email    = $email;
        $this->whatsapp = $whatsapp;
        $this->cv       = $cv;
    }

    /**
     * Função que faz toda a tratativa para subir o CV junto com os demais dados
     *
     * @return array
     */
    public function getResult()
    {
        /**
         * EXPLICAÇÃO
         * A função mb_strstr(<string>, <referencia>) retorna uma string a partir da referêcia que você determinar.
         * Ex: 
         * $cv = "meu-curriculo.pdf"
         * echo mb_strstr($cv, '.');
         * 
         * RETORNO: .pdf
         */
        $cvName = str_replace(mb_strstr($this->cv['name'], '.'), "", $this->cv['name']);
        $cvFile = $cvName . "_" . time() . mb_strstr($this->cv['name'], '.');
        $cvFile = str_replace('-', "_", $cvFile);
        $cvFile = str_replace(' ', "_", $cvFile);
        $cvFile = str_replace('  ', "_", $cvFile);

        $dir = "../22-curriculo/cv";

        /* A função [file_exists] é uma função integrada para verificar onde um diretório ou um arquivo existe ou não. Ela aceita um parâmetro de um caminho que retorna true se ele já existe ou false se não existir. */

        /* [is_dir] - Esta função também é semelhante a file_exists, e a única diferença é que ela só retornará true se a string passada for um diretório e retornará false se for um arquivo. */

        if (!file_exists($dir) || !is_dir($dir)) {

            // Crio o diretório com as permissões de Ler, Escrever e executar para DONO e Ler e Executar para todo os outros.
            mkdir($dir, 0755);
        }

        // Uso a função move_uploaded_file(), caso de certo a variável resultMoveFile recebe TRUE
        move_uploaded_file($this->cv['tmp_name'], "{$dir}/{$cvFile}");

        $all_data = [
            "name"     => $this->name,
            "email"    => $this->email,
            "whatsapp" => $this->whatsapp,
            "cv"       => $cvFile
        ];
        return $all_data;


        /**
         * [IMPORTANTE]
         * a função move_uploaded_file() em teoria era para retornar true, porém, o mesmo não está acontecendo.
         * Depois irei verificar melhor o motivo.
         */
    }
}
