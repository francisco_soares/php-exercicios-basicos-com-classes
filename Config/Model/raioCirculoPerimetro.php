<?php

namespace Config\Model;

class raioCirculoPerimetro
{
    // Atributos privados da classe
    private $num;
    private $result;

    public function setCalc($num)
    {
        $this->num = $num;
    }

    public function getRaio()
    {
        $this->result = pi() * $this->num * $this->num;
        return $this->result;
    }

    public function getPerimetro()
    {
        $this->result = 2 * pi() * $this->num;
        return $this->result;
    }
}
