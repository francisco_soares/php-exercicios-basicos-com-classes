<?php

namespace Config\Model;

class mes
{
    // Atributos privados
    private $number;
    private $month;
    private $result = 'n';

    public function __construct($number)
    {
        $this->number = $number;
    }

    public function getMonth()
    {
        $this->month = [
            '1'  => 'Janeiro',
            '2'  => 'Fevereiro',
            '3'  => 'Março',
            '4'  => 'Abril',
            '5'  => 'Maio',
            '6'  => 'Junho',
            '7'  => 'Julho',
            '8'  => 'Agosto',
            '9'  => 'Setembro',
            '10' => 'Outubro',
            '11' => 'Novembro',
            '12' => 'Dezembro'
        ];

        foreach ($this->month as $key => $value) {
            if ($this->number == $key) {
                $this->result = $value;
                break;
            }
        }

        return $this->result;
    }
}
