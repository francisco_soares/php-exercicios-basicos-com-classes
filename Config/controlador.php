<?php

use Config\Model\areaQuadrado;
use Config\Model\calculoHorasTrabalhadas;
use Config\Model\discountedSalary;
use Config\Model\envioImagem;
use Config\Model\GasolinaCalculo;
use Config\Model\joaoPescador;
use Config\Model\pesoIdeal;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

require_once "../vendor/autoload.php";

date_default_timezone_set('America/Sao_Paulo');

switch ($_POST['exercicio']) {
    case 'exercicio-1':

        /**
         * Exercício 1: Média do aluno
         */
        if ((isset($_POST['n1']) && empty($_POST['n1'])) || (isset($_POST['n2']) && empty($_POST['n2'])) || (isset($_POST['n3']) && empty($_POST['n3']))) {
            header("Location: ../1-media-do-aluno/index.php?result=error");
        } else {
            $mediaDoAluno = new \Config\Model\mediaDoAluno($_POST['n1'], $_POST['n2'], $_POST['n3']);

            $result = number_format($mediaDoAluno->getCalc(), 1);

            header("Location: ../1-media-do-aluno/index.php?result={$result}");
        }
        break;


    case 'exercicio-2':
        /**
         * Exercício 2: Calculadora
         */

        if ((isset($_POST['n1']) && empty($_POST['n1'])) || (isset($_POST['n2']) && empty($_POST['n2'])) || (isset($_POST['operador']) && empty($_POST['operador']))) {
            header("Location: ../2-calculadora/index.php?result=error");
        } else {
            $calculadora = new \Config\Model\Calculadora($_POST['n1'], $_POST['n2'], $_POST['operador']);

            if ($_POST['operador'] === "soma") {
                header("Location: ../2-calculadora/index.php?result={$calculadora->getCalc()}&operador={$_POST['operador']}");
            } elseif ($_POST['operador'] === "sub") {
                header("Location: ../2-calculadora/index.php?result={$calculadora->getCalc()}&operador={$_POST['operador']}");
            } elseif ($_POST['operador'] === "mult") {
                header("Location: ../2-calculadora/index.php?result={$calculadora->getCalc()}&operador={$_POST['operador']}");
            } else {
                header("Location: ../2-calculadora/index.php?result={$calculadora->getCalc()}&operador={$_POST['operador']}");
            }
        }
        break;

    case 'exercicio-3':
        /**
         * Exercício 3: Tabuada
         */
        if ((isset($_POST['n1']) && empty($_POST['n1'])) || (isset($_POST['n2']) && empty($_POST['n2']))) {
            header("Location: ../3-tabuada/index.php?result=error");
        } else {
            $tabuada = new \Config\Model\Tabuada;
            $tabuada->setTabuada($_POST['n1'], $_POST['n2']);

            $result = trim($tabuada->getTabuada());

            if ($tabuada->getTabuada()) {
                header("Location: ../3-tabuada/index.php?result={$result}&n1={$_POST['n1']}&n2={$_POST['n2']}");
            }
        }
        break;

    case 'exercicio-4':

        $num = (isset($_POST['num']) && !empty($_POST['num'])) ? $_POST['num'] : '';

        if (strpos($num, ',') >= 0 || strpos($num, '0,') >= 0) {
            $num = floatval(str_replace(',', '.', $num));
        } else if (strpos($num, '0.') >= 0 || strpos($num, '.') >= 0) {
            $num = floatval(str_replace('.', '', $num));
        }

        if (empty($num) || $num == 0) {
            header('Location: ../4-metros-para-centimetros/index.php?result=error');
        } else {

            $metroCentimetros = new \Config\Model\metrosParaCentimetros;

            $metroCentimetros->setNum($num);

            header("Location: ../4-metros-para-centimetros/index.php?result={$metroCentimetros->getNum()}&metros={$_POST['num']}");
        }
        break;

    case 'exercicio-5':

        $num = (isset($_POST['num']) && !empty($_POST['num']) ? $_POST['num'] : '');

        if (strpos($num, ',') >= 0 || strpos($num, '0,') >= 0) {
            $num = floatval(str_replace(',', '.', $num));
        } elseif (strpos($num, '.') >= 0 || strpos($num, '0.') >= 0) {
            $num = floatval(str_replace('.', '', $num));
        }

        if (empty($num) || $num == 0) {
            header('Location: ../5-raio-de-um-circulo/index.php?result=error');
        } else {
            $raioCirculoPerimetro = new \Config\Model\raioCirculoPerimetro;

            $raioCirculoPerimetro->setCalc($num);

            $raio = round($raioCirculoPerimetro->getRaio(), 2);
            $perimetro = round($raioCirculoPerimetro->getPerimetro(), 2);
            $raio = number_format($raio, 2, '.', '.');
            $perimetro = number_format($perimetro, 2, '.', '.');

            header("Location: ../5-raio-de-um-circulo/index.php?raio={$raio}&perimetro={$perimetro}");
        }

        break;

    case 'exercicio-6':
        $num = (isset($_POST['num']) && !empty($_POST['num'])) ? $_POST['num'] : '';

        if (strpos($num, ',') >= 0 || strpos($num, '0,') >= 0) {
            $num = floatval(str_replace(',', '.', $num));
        } elseif (strpos($num, '.') >= 0 || strpos($num, '0.') >= 0) {
            $num = floatval(str_replace('.', '', $num));
        }

        if (empty($num) || $num == 0) {
            header('Location: ../6-area-de-um-quadrado/index.php?result=error');
        } else {
            $areaQuadrado = new areaQuadrado;

            $areaQuadrado->setAreaQuadrado($num);

            $area = round($areaQuadrado->getAreaQuadrado(), 2);

            header("Location: ../6-area-de-um-quadrado/index.php?area={$area}");
        }
        break;

    case 'exercicio-7':
        $num = (isset($_POST['num']) && !empty($_POST['num'])) ? $_POST['num'] : '';

        if (strpos($num, ',') >= 0 || strpos($num, '0,') >= 0) {
            $num = floatval(str_replace(',', '.', $num));
        } elseif (strpos($num, '.') >= 0 || strpos($num, '0.') >= 0) {
            $num = floatval(str_replace('.', '', $num));
        }

        if (empty($num) || $num == 0) {
            header('Location: ../7-farenheit-para-celsius/index.php?result=error');
        } else {
            $farenheitParaCelsius = new \Config\Model\farenheitParaCelsius;

            $farenheitParaCelsius->setCalc($num);

            header("Location: ../7-farenheit-para-celsius/index.php?celsius={$farenheitParaCelsius->getCalc()}&farenheit={$num}");
        }
        break;

    case 'exercicio-8':
        $num = (isset($_POST['num']) && !empty($_POST['num'])) ? $_POST['num'] : '';

        if (strpos($num, ',') >= 0 || strpos($num, '0,') >= 0) {
            $num = floatval(str_replace(',', '.', $num));
        } elseif (strpos($num, '.') >= 0 || strpos($num, '0.') >= 0) {
            $num = floatval(str_replace('.', '', $num));
        }

        if (empty($num)) {
            header('Location: ../8-celsius-para-farenheit/index.php?result=error');
        } else {
            $celsiusParaFarenheit = new \Config\Model\celsiusParaFarenheit;

            $celsiusParaFarenheit->setCalc($num);

            header("Location: ../8-celsius-para-farenheit/index.php?celsius={$num}&farenheit={$celsiusParaFarenheit->getCalc()}");
        }
        break;

    case 'exercicio-9':
        $valor_hora = (isset($_POST['valor_hora']) && !empty($_POST['valor_hora'])) ? $_POST['valor_hora'] : '';
        $horas_mes = (isset($_POST['horas_mes']) && !empty($_POST['horas_mes'])) ? $_POST['horas_mes'] : '';

        if (strpos($valor_hora, ',') >= 0 || strpos($valor_hora, '0,') >= 0) {
            $valor_hora = floatval(str_replace(',', '.', $valor_hora));
        } elseif (strpos($horas_mes, ',') >= 0 || strpos($horas_mes, '0,') >= 0) {
            $horas_mes = floatval(str_replace(',', '.', $horas_mes));
        }

        if (empty($valor_hora) || empty($horas_mes)) {
            header('Location: ../9-salario-horas-mes/index.php?result=error');
        } else {
            $calculoHorasTrabalhadas = new calculoHorasTrabalhadas($valor_hora, $horas_mes);

            $calculoSalario = number_format($calculoHorasTrabalhadas->getCalc(), 2, ',', '.');

            header("Location: ../9-salario-horas-mes/index.php?result={$calculoSalario}&valor_hora={$valor_hora}&horas_mes={$horas_mes}");
        }

        break;

    case 'exercicio-10':
        $sexo = (isset($_POST['sexo']) && !empty($_POST['sexo'])) ? $_POST['sexo'] : '';
        $altura = (isset($_POST['altura']) && !empty($_POST['altura'])) ? $_POST['altura'] : '';

        if (strpos($altura, ',') >= 0) {
            $altura = str_replace(',', '', $altura);
        }

        if (strpos($altura, '.') >= 0) {
            $altura = str_replace('.', '', $altura);
        }

        if (strlen($altura) > 3) {
            header('Location: ../10-peso-ideal/index.php?result=maior');
        }

        $altura = floatval($altura);

        $posicao = substr($altura, 3);
        $recebe = str_replace($posicao, '', $altura);

        $substrAltura = floatval(substr_replace($recebe, '.', 1, 0));

        if ($substrAltura == '' || $substrAltura == 0 || $sexo == '') {
            header('Location: ../10-peso-ideal/index.php?result=error');
        } else {
            $pesoIdeal = new pesoIdeal($sexo, $substrAltura);

            $alturaFloat = substr_replace($altura, '.', 1, 0);

            header("Location: ../10-peso-ideal/index.php?result={$pesoIdeal->getCalc()}&sexo={$sexo}&altura={$alturaFloat}");
        }
        break;

    case 'exercicio-11':

        $peso = (isset($_POST['peso']) && !empty($_POST['peso'])) ? $_POST['peso'] : '';

        if (strpos($peso, ',') > 0) {
            $pesoPeixe = str_replace(',', '.', $peso);
        }

        $pesoPeixe = floatval($peso);

        if ($pesoPeixe == 0) {
            header("Location: ../11-joao-pescador/index.php?result=error");
        }

        $joaoPescador = new joaoPescador($pesoPeixe);

        if (!empty($joaoPescador->getCalc())) {
            extract($joaoPescador->getCalc());

            $fineAmountFormat = number_format($fineAmount, 2, ',', '.');

            header("Location: ../11-joao-pescador/index.php?weight={$weight}&weightExceeded={$weightExceeded}&fineAmount={$fineAmountFormat}");
        }

        break;

    case 'exercicio-12':
        $value_hour = (isset($_POST['value_hour']) && !empty($_POST['value_hour'])) ? $_POST['value_hour'] : '';
        $month_worked = (isset($_POST['month_worked']) && !empty($_POST['month_worked'])) ? $_POST['month_worked'] : '';

        if (strpos($value_hour, ',') && strpos($month_worked, ',')) {
            $value_hour = str_replace(',', '.', $value_hour);
            $month_worked = str_replace(',', '.', $month_worked);
        }

        $value_hour_float = floatval($value_hour);
        $month_worked_float = floatval($month_worked);

        if ($value_hour_float == 0 || $month_worked_float == 0) {
            header("Location: ../12-salario-dos-com-descontos/index.php?result=error");
        }

        $discountedSalary = new discountedSalary($value_hour_float, $month_worked_float);

        if (!empty($discountedSalary->getCalc())) {
            extract($discountedSalary->getCalc());

            $gross_salary_format = number_format($gross_salary, 2, ',', '.');
            $net_salary_format = number_format($net_salary, 2, ',', '.');
            $ir_format = number_format($ir, 2, ',', '.');
            $inss_format = number_format($inss, 2, ',', '.');
            $syndicate_format = number_format($syndicate, 2, ',', '.');

            header("Location: ../12-salario-dos-com-descontos/index.php?gross_salary={$gross_salary_format}&net_salary={$net_salary_format}&ir={$ir_format}&inss={$inss_format}&syndicate={$syndicate_format}");
        }

        break;

    case 'exercicio-13':
        $number_1 = (isset($_POST['number_1']) && !empty($_POST['number_1']) ? $_POST['number_1'] : '');
        $number_2 = (isset($_POST['number_2']) && !empty($_POST['number_2'])) ? $_POST['number_2'] : '';

        if (strpos($number_1, ',') && strpos($number_2, ',')) {
            $number_1 = str_replace(',', '.', $number_1);
            $number_2 = str_replace(',', '.', $number_2);
        }

        $number_1_float = floatval($number_1);
        $number_2_float = floatval($number_2);

        if ($number_1_float <= 0 || $number_2_float <= 0) {
            header("Location: ../13-soma-dois-numeros/index.php?result=error");
        }

        $somaDoisNumeros = new \Config\Model\somaDoisNumeros($number_1_float, $number_2_float);

        if (!empty($somaDoisNumeros->getCalc())) {
            extract($somaDoisNumeros->getCalc());

            $number_format = number_format($total, 2, ',', '.');

            header("Location: ../13-soma-dois-numeros/index.php?total=$number_format&operator=$operator&assigned=$assigned&number_1_float=$number_1_float&number_2_float=$number_2_float");
        }

        break;

    case 'exercicio-14':

        $number_1 = (isset($_POST['number']) && !empty($_POST['number']) ? $_POST['number'] : '');

        if (strpos($number_1, ',')) {
            $number_1 = str_replace(',', '.', $number_1);
        }

        $number_1_float = floatval($number_1);

        if ($number_1_float <= 0) {
            header("Location: ../14-divisivel-por-10-5-2/index.php?result=error");
            die;
        }

        $divisivelPorDezCincoDois = new \Config\Model\divisivelPorDezCincoDois($number_1_float);

        if (!empty($divisivelPorDezCincoDois->getCalc())) {
            extract($divisivelPorDezCincoDois->getCalc());

            header("Location: ../14-divisivel-por-10-5-2/index.php?result_two=$result_two&result_five=$result_five&result_ten=$result_ten&number_send=$number_send");
        }

        break;

    case 'exercicio-15':

        $number = (isset($_POST['number']) && !empty($_POST['number']) ? $_POST['number'] : '');

        $number_int = intval($number);

        if ($number_int <= 0) {
            header("Location: ../15-mes/index.php?result=error");
            die;
        }

        $mes = new \Config\Model\mes($number_int);

        if ($mes->getMonth()) {
            header("Location: ../15-mes/index.php?month={$mes->getMonth()}");
        }
        break;

    case 'exercicio-16':
        $name = (isset($_POST['name']) && !empty($_POST['name'])) ? $_POST['name'] : '';
        $book = (isset($_POST['book']) && !empty($_POST['book'])) ? $_POST['book'] : '';
        $choice = (isset($_POST['choice']) && !empty($_POST['choice'])) ? $_POST['choice'] : '';

        if ($name == "" || $book == "" || $choice == "") {
            header("Location: ../16-biblioteca-universidade/index.php?result=error");
            die;
        }

        $bibliotecaUniversidade = new \Config\Model\bibliotecaUniversidade($name, $book, $choice);

        if ($bibliotecaUniversidade->result()) {
            extract($bibliotecaUniversidade->result());
            header("Location: ../16-biblioteca-universidade/index.php?days={$days}&name={$name}&book={$book}");
        }

        break;

    case 'exercicio-17':
        $word = isset($_POST['word']) && !empty($_POST['word']) ? $_POST['word'] : '';

        if ($word == "") {
            header("Location: ../17-impressao-de-palavra/index.php?result=error");
        }

        $impressaoPalavra = new \Config\Model\impressaoPalavra($word);

        header("Location: ../17-impressao-de-palavra/index.php?word={$impressaoPalavra->getWord()}");
        break;

    case 'exercicio-18':
        $book_title  = trim((isset($_POST['book_title']) && $_POST['book_title'] != "") ? $_POST['book_title'] : '');
        $genero_book = trim((isset($_POST['genero_book']) && $_POST['genero_book'] != "") ? $_POST['genero_book'] : '');
        $author_book = trim((isset($_POST['author_book']) && $_POST['author_book'] != "") ? $_POST['author_book'] : '');
        $price_book  = trim((isset($_POST['price_book']) && $_POST['price_book'] != "") ? $_POST['price_book'] : "");

        if ($book_title == "" || $genero_book == "" || $author_book == "" || $price_book == "") {
            header("Location: ../18-arquivo-xml/index.php?result=error");
        }

        $arquivoXml = new \Config\Model\arquivoXml($book_title, $genero_book, $author_book, $price_book);

        if ($arquivoXml->getBook() != false) {

            $file_book = "../18-arquivo-xml/xml/livro_" . strtolower(str_replace(' ', '_', $book_title)) . ".xml";

            if (!file_exists($file_book)) {

                /** Aqui crio e salvo um arquivo XML */

                // DOMDocument('<xml-version>, <encode>') 
                $dom = new DOMDocument('1.0', 'UTF-8');

                // Formata a saida do XML
                $dom->formatOutput = true;

                // Para escrever em um nó, utilize o $dom->createTextNode('<description>') ou $dom->createCDATASection('<description>') caso seja um campo com caracteres especiais.

                // Nó de titulo do livro
                $book_title_text = $dom->createCDATASection($arquivoXml->getBook()['book_title']);
                $book_t = $dom->createElement('book_title');
                $book_t->appendChild($book_title_text);

                // Nó de gênero do livro
                $book_genero_text = $dom->createCDATASection($arquivoXml->getBook()['genero_book']);
                $book_g = $dom->createElement('genero_book');
                $book_g->appendChild($book_genero_text);

                // Nó de author do livro
                $book_author_text = $dom->createCDATASection($arquivoXml->getBook()['author_book']);
                $book_a = $dom->createElement('author_book');
                $book_a->appendChild($book_author_text);

                // Nó de author do livro
                $book_price_text = $dom->createCDATASection($arquivoXml->getBook()['price_book']);
                $book_p = $dom->createElement('price_book');
                $book_p->appendChild($book_price_text);

                // Nó do titulo do livro
                $content_book = $dom->createElement('content_book');
                $content_book->appendChild($book_t);
                $content_book->appendChild($book_g);
                $content_book->appendChild($book_a);
                $content_book->appendChild($book_p);

                // Nó root/raiz - Nó principal do XML
                $rootNode = $dom->createElement('root');
                $rootNode->appendChild($content_book);

                // Adiciona o Nó criado ao XML
                $dom->appendChild($rootNode);

                // Imprime XML na tela
                // echo $dom->saveXML();
                // echo "===================================";
                // var_dump($dom->saveXML());

                // Salva o conteúdo em um arquivo
                $dom->save($file_book);

                header("Location: ../18-arquivo-xml/index.php?result=saved_book&name_book=" . $book_title);
            } else {
                header("Location: ../18-arquivo-xml/index.php?result=book_exists&name_book=" . $book_title);
            }
        } else {
            header("Location: ../18-arquivo-xml/index.php?result=some_error&name_book=" . $book_title);
        }
        break;

    case 'exercicio-19':
        $metro_quadrado = trim(isset($_POST['metro_quadrado']) && $_POST['metro_quadrado'] != "") ? $_POST['metro_quadrado'] : '';
        $metro_quadrado_floate = floatval($metro_quadrado);

        if ($metro_quadrado_floate == "" || $metro_quadrado_floate == null || $metro_quadrado_floate == 0) {
            header('Location: ../19-loja-tintas/index.php?result=error');
            die;
        }

        if (strpos($metro_quadrado_floate, ',')) {
            $metro_quadrado_floate = str_replace(",", '.', $metro_quadrado_floate);
        }

        $lojaTintas = new \Config\Model\lojaTintas($metro_quadrado_floate);

        if ($lojaTintas->result()) {
            extract($lojaTintas->result());
            header("Location: ../19-loja-tintas/index.php?qtd_litro={$qtd_litro}&qtd_latas={$qtd_latas}&valor_total={$valor_total}");
        }
        break;

    case 'exercicio-20':
        $number = trim(isset($_POST['number'])) && $_POST['number'] != "" ? $_POST['number'] : '';
        $arr_number_100_200 = [];

        if ($number == "" || $number == null) {
            header("Location: ../20-numero-entre-100-e-200/index.php?result=error");
        }

        $number_explode = explode(',', $number);

        $read_number = new \Config\Model\lerNumeros($number_explode);

        foreach ($read_number->getResult() as $r_number) {

            /* Uso o floatval() no $r_number pois se retornar um valo = 100 ola, o mesmo vai converter para 0, assim não entra no if. */
            if (floatval($r_number) >= 100 && floatval($r_number) <= 200) {
                array_push($arr_number_100_200, $r_number);
            } else if (floatval($r_number) == 0) {
                die;
            }
        }

        /* Dou um implde() separando o array em , */
        $implode_number_100_200 = implode(',', $arr_number_100_200);

        header("Location: ../20-numero-entre-100-e-200/index.php?result=success&arr_number_100_200=$implode_number_100_200");

        break;

    case "exercicio-21":
        $height = trim(isset($_POST['height'])) && $_POST['height'] != "" ? $_POST['height'] : '';
        $height_you_want = trim(isset($_POST['height_you_want'])) && $_POST['height_you_want'] != "" ? $_POST['height_you_want'] : '';

        if ($height == "" || $height == null || $height_you_want == "" || $height_you_want == null) {
            header("Location: ../21-altura/index.php?result=error");
            die;
        }

        if (!strpos($height, '.')) {
            $height = substr_replace($height, '.', 1, 0);
        }

        if (!strpos($height_you_want, '.')) {
            $height_you_want = substr_replace($height_you_want, '.', 1, 0);
        }


        $float_height = floatval($height);
        $float_height_you_want = floatval($height_you_want);

        $person = new \Config\Model\Altura($float_height, $float_height_you_want);

        header("Location: ../21-altura/index.php?result=success&year=" . $person->getResult()['year'] . "&current_height=" . $person->getResult()['current_height'] . "&intended_height=" . $person->getResult()['intended_height'] . "&height_you_want=" . $height_you_want);

        break;

    case 'exercicio-22':
        $name = isset($_POST['name']) && $_POST['name'] != "" ? $_POST['name'] : '';
        $email = isset($_POST['email']) && $_POST['email'] != "" ? $_POST['email'] : '';
        $whatsapp = isset($_POST['whatsapp']) && $_POST['whatsapp'] != "" ? $_POST['whatsapp'] : '';
        $cv = isset($_FILES['cv']) && $_FILES['cv'] != "" ? $_FILES['cv'] : '';
        $size_file = 150000;

        /* [FILE] só funciona com método [POST] definido no Formulário */
        /* O [FILE] retorna as seguintes informações no [Array]
            [name]     - Nome completo do arquivo com a extensão
            [type]     - Tipo do arquivo. Caso seja enviado um arquivo PDF o tipo será [application/pdf], PNG [image/png], JPG [image/jpeg]
            [tmp_name] - é o local onde o arquivo fica salvo TEMPORÁRIAMENTE
            [error]    - Retorna um código de erro caso tenha um problema de um erro
            [size]     - Retorna o tamanho do arquivo  
        */
        /* MIME TYPE */
        // Abaixo tenho uma pequena lista de MIME TYPE, no arquivo [mime-tipe.md] tem uma lista com mais.
        // $pdf  = "application/pdf";
        // $docx = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
        // $doc  = "application/msword";
        // $jpeg = "image/jpeg";
        // $png  = "image/png";
        // $gif  = "image/gif";

        $cvExtensoes = [
            "application/pdf",
            "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
            "application/msword"
        ];

        if ($name == "" || $email == "" || $whatsapp == "" || $cv == "") {
            header("Location: ../22-curriculo/index.php?result=error");
            die;
        }

        if (!in_array($cv['type'], $cvExtensoes)) {
            header("Location: ../22-curriculo/index.php?result=errorCV");
            die;
        }

        if ($cv['size'] < $size_file && in_array($cv['type'], $cvExtensoes)) {

            $person = new \Config\Model\Curriculo($name, $email, $whatsapp, $cv);

            header("Location: ../22-curriculo/index.php?result=success&name=" . $person->getResult()['name'] . "&email=" . $person->getResult()['email'] . "&whatsapp=" . $person->getResult()['whatsapp'] . "&cv=" . $person->getResult()['cv']);
        }

        break;

    case "exercicio-23":
        $file_name = isset($_POST['file_name']) ? $_POST['file_name'] : "";
        $file_img  = isset($_FILES['file_img']) ? $_FILES['file_img'] : "";
        $size_file = 150000;

        if ($file_img == "") {
            header("Location: ../23-enviando-imagem/index.php?result=errorEmpty");
            die;
        }

        /* [FILE] só funciona com método [POST] definido no Formulário */
        /* O [FILE] retorna as seguintes informações no [Array]
            [name]     - Nome completo do arquivo com a extensão
            [type]     - Tipo do arquivo. Caso seja enviado um arquivo PDF o tipo será [application/pdf], PNG [image/png], JPG [image/jpeg]
            [tmp_name] - é o local onde o arquivo fica salvo TEMPORÁRIAMENTE
            [error]    - Retorna um código de erro caso tenha um problema de um erro
            [size]     - Retorna o tamanho do arquivo  
        */
        /* MIME TYPE */
        // Abaixo tenho uma pequena lista de MIME TYPE, no arquivo [mime-tipe.md] tem uma lista com mais.
        // $pdf  = "application/pdf";
        // $docx = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
        // $doc  = "application/msword";
        // $jpeg = "image/jpeg";
        // $png  = "image/png";
        // $gif  = "image/gif";

        $imgExtensoes = [
            "image/jpeg",
            "image/png",
            "image/gif"
        ];

        if (!in_array($file_img['type'], $imgExtensoes)) {
            header("Location: ../23-enviando-imagem/index.php?result=errorFormat");
            die;
        }

        if ($file_img['size'] < $size_file && in_array($file_img['type'], $imgExtensoes)) {
            $objImg = new \Config\Model\envioImagem($file_name, $file_img);

            if ($objImg->getResult() == true) {
                header("Location: ../23-enviando-imagem/index.php?result=success");
            } else {
                header("Location: ../23-enviando-imagem/index.php?result=erro-upload");
            }
        }
        break;

    case 'trekking':
        $jSON = [];
        $data = [];
        $file = fopen($_FILES['abbr']['tmp_name'], 'r');

        $fileExtension = [
            "text/csv",
            "image/jpeg",
            "image/png",
            "image/gif"
        ];

        if (!in_array($_FILES['abbr']['type'], $fileExtension)) {
            $jSON['status'] = false;
            echo json_encode($jSON);
            http_response_code(202);
            die;
        }

        $insert = new \Config\Helper\Create;
        // $insert->goCreate('trekking_abbr');

        while ($file_data = fgetcsv($file, 1000, ',')) {
            $data['code']   = $file_data[0];
            $data['name']   = $file_data[1];
            $data['abbr']   = $file_data[2];
            $data['status'] = '1';
            $data['created_at'] = date("Y-m-d H:i:s");

            $insert->goCreate('trekking_abbr', $data);

            if ($insert->getResult() < 1) {
                $jSON['status'] = false;
                $jSON['message'] = "Erro ao cadastrar o UF";
                echo json_encode($jSON);
                http_response_code(202);
                die;
            }
        }
        $jSON['status'] = true;
        echo json_encode($jSON);
        // http_response_code(200);
        break;

    case 'exercicio-27':

        $arr_title = [
            'A1' => 'NOME',
            'B1' => 'E-MAIL',
            'C1' => 'CPF',
            'D1' => 'DATA DE NASCIMENTO',
            'E1' => 'ESCOLARIDADE'
        ];

        $arr_txt = [
            'person1' => [
                'A2' => 'Saitama',
                'B2' => 'saitama@teste.com.br',
                'C2' => '398.563.222-89',
                'D2' => '05/05/1985',
                'E2' => 'Ensino Superior Completo'
            ],
            'person2' => [
                'A2' => 'Homer Simpson',
                'B2' => 'homer-simpson@teste.com.br',
                'C2' => '398.563.222-90',
                'D2' => '06/06/1996',
                'E2' => 'Ensino Técnico'
            ],
        ];

        $spreadsheet = new Spreadsheet();
        $activeWorksheet = $spreadsheet->getActiveSheet();

        foreach ($arr_title as $cell => $value) {
            $activeWorksheet->setCellValue($cell, $value);
        }

        foreach ($arr_txt as $person => $data) {
            foreach ($data as $cell => $value) {
                $activeWorksheet->setCellValue($cell, $value);
            }
        }

        $filename = 'person.xlsx';

        // header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        // header("Content-Disposition: attachment; filename=\"{$filename}\"");
        // header('Cache-Control: max-age=0');

        // Envia o arquivo para o navegador
        $writer = new Xlsx($spreadsheet);
        $writer->save('../27-excel/person.xlsx');
        // $writer->save('php://output');

        header("Location: ../27-excel/index.php?result=success");
        break;


    case 'exercicio-28':

        $jSON = [];

        if($_POST['radio_select'] == 'litro') {
            $gasolina_calculo = new GasolinaCalculo($_POST['preco_litro'], $_POST['distancia']);
            $jSON['preco'] = $gasolina_calculo->getCalcLitro();
            $jSON['litros'] = $_POST['distancia'];
        } else {
            $gasolina_calculo = new GasolinaCalculo($_POST['preco_litro'], $_POST['distancia'], $_POST['consumo']);
            $jSON['preco'] = $gasolina_calculo->getCalcConsumo();
            $jSON['consumo'] = $gasolina_calculo->getConsumo();
            $jSON['km'] = $gasolina_calculo->getKm();
        }
        

        
        $jSON['status'] = true;
        
        http_response_code(202);
        
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($jSON);
        die;

    default:
        header('Location: ../');
        break;
}
