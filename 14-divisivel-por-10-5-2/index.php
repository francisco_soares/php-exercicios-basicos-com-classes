<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Divide por 10, 5 e 2</title>

    <!-- Style -->
    <link rel="stylesheet" href="../assets/style/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/style/style.css?v=<?= time(); ?>">
</head>

<body>

    <div class="container h-100">
        <div class="row h-100">
            <div class="formulario col-md-6 offset-md-3 align-self-center" id="content">
                <h3 class="text-center font-weight-bold">Divide por 10, 5 e 2</h3>
                <p class="text-center">Informe o número abaixo e veja se é divisivel por 10, 5 e 2</p>

                <form action="../Config/controlador.php" method="POST" id="sendForm">
                    <input type="hidden" name="exercicio" value="exercicio-14">

                    <div class="mb-4">
                        <label for="">Informe um número</label>
                        <input type="text" name="number" id="number" class="form-control" placeholder="Ex: 10">
                    </div>

                    <div class="mb-4">
                        <input type="submit" value="Enviar" id="btn_send" class="btn btn-lg btn-outline-danger w-100 font-weight-bold">
                    </div>
                </form>


                <?php if (isset($_GET['result']) && $_GET['result'] == "error") { ?>
                    <div class="bg-warning p-3 rounded text-center result">
                        <h3 class="text-dark">OPSS...</h3>
                        <p class="text-dark"><strong>TODOS</strong> os campos devem ser preenchido!</p>
                    </div>
                <?php } ?>

                <?php if (isset($_GET['result_two'])  && isset($_GET['result_five']) && isset($_GET['result_ten'])) { ?>
                    <div class="bg-success p-3 rounded text-center result">
                        <h3 class="text-white">Resultado</h3>
                        <p class="text-white">
                            <?= $_GET['number_send'] ?> é divisível por 2: <strong><?= (isset($_GET['result_two']) && $_GET['result_two'] == true) ? 'Sim' : 'Não'; ?></strong> <br>
                            <?= $_GET['number_send'] ?> é divisível por 5: <strong><?= (isset($_GET['result_five']) && $_GET['result_five'] == true) ? 'Sim' : 'Não'; ?></strong> <br>
                            <?= $_GET['number_send'] ?> é divisível por 10: <strong><?= (isset($_GET['result_ten']) && $_GET['result_ten'] == true) ? 'Sim' : 'Não'; ?></strong> <br>
                        </p>
                    </div>
                <?php } ?>

            </div>
        </div>
    </div>


    <!-- Scripts -->
    <script src="../assets/style/bootstrap/js/bootstrap.min.js"></script>
    <script src="../assets/js/jquery-3.6.0.min.js"></script>

    <script>
        /**
         * Desativa o botão assim que é iniciado a aplicação.
         */
        $(document).ready(function() {
            let number = $('#number').val()

            if (number == "") {
                $('#btn_send').prop('disabled', true)
            }
        })

        $('#sendForm').keyup(function() {
            let number = $('#number').val()
            let number_float = parseFloat(number)

            // Verifica se existe valor digitado, caso tenha, habilita o btn.
            if (number || !number) {
                $("#btn_send").prop('disabled', false)
            }

            // Desabilita o btn de enviar o formulário caso não exista valor digitado.
            if (number == "") {
                $("#btn_send").prop("disabled", true)
            }

            if ($('#errorForm')) {
                $('#errorForm').remove()
            }

            if (isNaN(number)) {
                $("#btn_send").prop("disabled", true)

                let returnError = $("<div>", {
                    id: "errorForm",
                    class: "bg-danger p-3 rounded text-center"
                })

                if($('.result')){
                    $('.result').remove()
                }

                returnError.html('<h3 class="text-white">Éhhh...</h3><p class="text-white">Insira apenas <strong>Números</strong>!</p>')
                $("#content").append(returnError)
            }
        })
    </script>
</body>

</html>