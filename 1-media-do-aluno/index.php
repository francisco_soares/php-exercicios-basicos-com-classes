<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Exercício 1 - Média do Aluno</title>

    <!-- Style -->
    <link rel="stylesheet" href="../assets/style/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/style/style.css?v=<?= time(); ?>">
</head>

<body>

    <div class="container h-100">
        <div class="row h-100">
            <div class="formulario col-md-6 offset-md-3 align-self-center" id="conteudo">
                <h3 class="text-center font-weight-bold text-uppercase">Calculo da média do Aluno</h3>
                <p class="text-center mb-4">Informe as notas do aluno</p>

                <form action="../Config/controlador.php" method="POST" id="enviarForm">
                    <input type="hidden" name="exercicio" value="exercicio-1">
                    <div class="mb-3">
                        <label class="font-weight-bold">1ª Nota</label>
                        <input type="text" name="n1" id="n1" class="form-control" placeholder="Informe a 1ª Nota.">
                    </div>
                    <div class="mb-3">
                        <label class="font-weight-bold">2ª Nota</label>
                        <input type="text" name="n2" id="n2" class="form-control" placeholder="Informe a 2ª Nota.">
                    </div>
                    <div class="mb-3">
                        <label for="" n3 class="font-weight-bold">3ª Nota</label>
                        <input type="text" name="n3" id="n3" class="form-control" placeholder="Informe a 3ª Nota">
                    </div>

                    <div class="mb-3">
                        <input type="submit" value="Enviar" class="btn btn-outline-danger btn-lg w-100 text-uppercase font-weight-bold">
                    </div>
                </form>

                <?php
                if (isset($_GET['result'])) {
                    if ($_GET['result'] == "error") {
                ?>
                        <div class="bg-warning p-3 rounded text-center">
                            <h3 class="text-dark">OPSS...</h3>
                            <p class="text-dark"><strong>TODOS</strong> os campos devem ser preenchido!</p>
                        </div>
                    <?php
                    } elseif (isset($_GET['result']) && $_GET['result'] < 6) {
                    ?>
                        <div class="bg-danger p-3 rounded text-center">
                            <h3 class="text-white">Resultado</h3>
                            <p class="text-white">Sua média é <strong><?= $_GET['result']; ?></strong>. Você está reprovado.</p>
                        </div>
                    <?php
                    } else {
                    ?>
                        <div class="bg-success p-3 rounded text-center">
                            <h3 class="text-white">Resultado</h3>
                            <p class="text-white">Sua média é <strong><?= $_GET['result']; ?></strong>. Você está Aprovado.</p>
                        </div>
                <?php
                    }
                } ?>
            </div>
        </div>
    </div>


    <script src="../assets/style/bootstrap/js/bootstrap.min.js"></script>
    <script src="../assets/js/jquery-3.6.0.min.js"></script>

    <script>
        /**
         * Pego a ação de submit do formulário
         */
        $('#enviarForm').submit(function(e) {

            // Pego os valores dos campos do formulário
            let n1 = parseFloat($('#n1').val())
            let n2 = parseFloat($('#n2').val())
            let n3 = parseFloat($('#n3').val())

            /**
             * Verifico se os valores passado é NaN(Not-A-Number) ou vazio, atenda uma dessa condição, entra dentro do laço.
             */
            if ((isNaN(n1) || n1 == "") || (isNaN(n2) || n2 == "") || (isNaN(n3) || n3 == "")) {

                // Paro a ação de submit do formulário.
                e.preventDefault()

                // Crio uma DIV com ID e CLASSES
                let returnError = $("<div>", {
                    id: 'errorForm',
                    class: 'bg-danger p-3 rounded text-center'
                })

                // Verifico se a div ID #errorForm exista. Caso exista, entro na condição e removo.
                if ($('#errorForm')) {
                    $('#errorForm').remove()
                }

                // Insiro alguns textos na DIV #errorForm que defini acima.
                returnError.html('<h3 class="text-white">Éhhh...</h3><p class="text-white">Insira apenas <strong>Números</strong>!</p>')

                // Insiro a DIV #errorForm após todos os filhos da DIV ID pai #conteudo
                $("#conteudo").append(returnError)

                let out
                out = setTimeout(alertOut, 5000)
            }
        })

        /**
         * Função que apenas apaga a DIV         
         */
        function alertOut() {
            $("#errorForm").fadeOut(1000)
        }
    </script>

</body>

</html>