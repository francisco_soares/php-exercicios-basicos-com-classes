<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Soma de dois números</title>

    <!-- Style -->
    <link rel="stylesheet" href="../assets/style/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/style/style.css?v=<?= time(); ?>">
</head>

<body>

    <div class="container h-100">
        <div class="row h-100">
            <div class="formulario col-md-6 offset-md-3 align-self-center" id="content">
                <h3 class="text-center font-weight-bold">Soma de dois Números</h3>
                <p class="text-center">Digite dois números para ser feito os cálculos.</p>

                <form action="../Config/controlador.php" method="POST" id="sendForm">

                    <input type="hidden" name="exercicio" value="exercicio-13">
                    <div class="mb-4">
                        <label for="number-1">Informe o 1º número</label>
                        <input type="text" name="number_1" value="<?= isset($_GET['number_1_float']) && !empty($_GET['number_1_float']) ? $_GET['number_1_float'] : ''; ?>" id="number_1" class="form-control" placeholder="Ex: 5">
                    </div>

                    <div class="mb-4">
                        <label for="number-2">Informe o 2º número</label>
                        <input type="text" name="number_2" value="<?= isset($_GET['number_2_float']) && !empty($_GET['number_2_float']) ? $_GET['number_2_float'] : ''; ?>" id="number_2" class="form-control" placeholder="Ex: 5">
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="mb-4">
                                <label for="result_calc">Soma dos valores digitados</label>
                                <input type="text" name="result_calc" value="<?= isset($_GET['total']) && !empty($_GET['total']) ? $_GET['total'] : ''; ?>" id="result_calc" class="form-control" disabled>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="mb-4">
                                <label for="value_withdrawn_inserted">Foi <span id="operator" class="font-weight-bold"><?= isset($_GET['operator']) && !empty($_GET['operator']) ? $_GET['operator'] : ''; ?></span></label>
                                <input type="text" name="value_withdrawn_inserted" value="<?= isset($_GET['assigned']) && !empty($_GET['assigned']) ? $_GET['assigned'] : ''; ?>" id="value_withdrawn_inserted" class="form-control" disabled>
                            </div>
                        </div>
                    </div>

                    <div class="mb-4">
                        <input type="submit" value="Enviar" id="btn_send" class="btn btn-lg btn-outline-danger w-100 font-weight-bold">
                    </div>
                </form>

                <?php if (isset($_GET['result']) && $_GET['result'] == "error") { ?>
                    <div class="bg-warning p-3 rounded text-center result">
                        <h3 class="text-dark">OPSS...</h3>
                        <p class="text-dark"><strong>TODOS</strong> os campos devem ser preenchido!</p>
                    </div>
                <?php } ?>
                <?php if ((isset($_GET['total']) && !empty($_GET['total'])) && (isset($_GET['operator']) && !empty($_GET['operator']))) { ?>
                    <div class="bg-success p-3 rounded text-center result">
                        <h3 class="text-white">Resultado</h3>
                        <p class="text-white">
                            <strong>Total: </strong> <?= $_GET['total']; ?> <br>
                            Foi <strong><?= $_GET['operator']; ?> <?= $_GET['assigned'] ?></strong> ao resultado. <br>
                        </p>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>

    <!-- Scripts -->
    <script src="../assets/style/bootstrap/js/bootstrap.min.js"></script>
    <script src="../assets/js/jquery-3.6.0.min.js"></script>

    <script>
        $(document).ready(function() {
            
            let number_1 = $("#number_1").val()
            let number_2 = $("#number_2").val()

            if (number_1 = "" || number_2 == "") {
                $('#btn_send').prop('disabled', true)
            }
        })
        $('#sendForm').keyup(function() {
            // Variaveis
            let number_1 = $("#number_1").val()
            let number_2 = $("#number_2").val()
            let number_1_float = parseFloat(number_1)
            let number_2_float = parseFloat(number_2)
            let total = number_1_float + number_2_float
            let result

            if ((number_1 || !number_1) || (number_2 || !number_2)) {
                $('#btn_send').prop('disabled', false)
            }

            if(number_1 == "" || number_2 == ""){
                $("#btn_send").prop("disabled", true)
            }

            if ($("#errorForm")) {
                $("#errorForm").remove()
            }

            if (isNaN(number_1) || isNaN(number_2)) {

                $("#btn_send").prop("disabled", true)

                let returnError = $("<div>", {
                    id: 'errorForm',
                    class: 'bg-danger p-3 rounded text-center'
                })

                returnError.html('<h3 class="text-white">Éhhh...</h3><p class="text-white">Insira apenas <strong>Números</strong>!</p>')

                $("#content").append(returnError)

                if ($('.result')) {
                    $('.result').remove()
                }

                // Limpo os campos onde não da para editar os valores
                $('#result_calc').val('')
                $('#operator').html('...')
                $('#value_withdrawn_inserted').val('')

            } else {
                if (!isNaN(number_1) || !isNaN(number_2)) {
                    if (number_1_float > 0 && number_2_float > 0) {
                        total = number_1_float + number_2_float

                        if (total > 20) {
                            result = total + 8
                            $('#result_calc').val(result)
                            $('#operator').html('somado')
                            $('#value_withdrawn_inserted').val(8)

                        } else {
                            result = total - 5
                            $('#result_calc').val(result)
                            $('#operator').html('subtraido')
                            $('#value_withdrawn_inserted').val(5)
                        }
                    }
                }
            }

            if (number_1 == "" && number_2 == "") {
                $('#result_calc').val('')
                $('#operator').html('...')
                $('#value_withdrawn_inserted').val('')
            }
        })
    </script>
</body>

</html>