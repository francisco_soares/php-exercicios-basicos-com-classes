<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Raio de um Circulo</title>

    <!-- Style -->
    <link rel="stylesheet" href="../assets/style/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../assets/style/style.css?v=<?= time(); ?>">
</head>

<body>
    <div class="container h-100">
        <div class="row h-100">
            <div class="formulario col-md-6 offset-md-3 align-self-center  text-center" id="conteudo">
                <h3 class="text-dark font-weight-bold">Raio de um Circulo</h3>
                <p class="text-dark">Informe o raio do circulo no formulário abaixo.</p>

                <form action="../Config/controlador.php" method="post" id="enviarForm">
                    <input type="hidden" name="exercicio" value="exercicio-5">
                    <div class="mb-3">
                        <label for="num" class="font-weight-bold text-left">Informe um numero</label>
                        <input type="text" name="num" id="num" class="form-control form-control-lg" placeholder="ex: 50">
                    </div>

                    <div class="mb-3">
                        <input type="submit" value="Enviar" class="btn btn-lg btn-outline-danger w-100 font-weight-bold text-uppercase">
                    </div>
                </form>

                <?php if (isset($_GET['result']) && $_GET['result'] == 'error') { ?>
                    <div class="bg-warning p-3 rounded text-center result">
                        <h3 class="text-dark">OPSS...</h3>
                        <p class="text-dark"><strong>TODOS</strong> os campos devem ser preenchido!</p>
                    </div>
                <?php } else { ?>
                    <div class="bg-success p-3 rounded text-center result">
                        <h3 class="text-white">Resultado</h3>
                        <p class="text-white">
                            <strong>Raio: </strong> <?= $_GET['raio']; ?><br>
                            <strong>Perimetro: </strong> <?= $_GET['perimetro']; ?>
                        </p>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>

    <!-- Scripts -->
    <script src="../assets/style/bootstrap/js/bootstrap.min.js"></script>
    <script src="../assets/js/jquery-3.6.0.min.js"></script>

    <script>
        $("#enviarForm").submit(function(e) {

            // Pego o valor enviado pelo formulário
            let num = parseFloat($('#num').val())

            if (isNaN(num)) {
                e.preventDefault()

                let returnError = $('<div>', {
                    id: 'errorForm',
                    class: 'bg-danger p-3 rounded text-center'
                })

                if ($("#errorForm")) {
                    $("#errorForm").remove()
                }

                returnError.html('<h3 class="text-white">Éhhh...</h3><p class="text-white">Insira apenas <strong>Números</strong>!</p>')

                $("#conteudo").append(returnError)

                if ($('.result')) {
                    $('.result').remove()
                }
            }

            let out

            out = setTimeout(alertOut, 5000)
        })

        function alertOut() {
            $("#errorForm").fadeOut(800)
        }
    </script>
</body>

</html>